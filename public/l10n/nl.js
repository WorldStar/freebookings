{

"header" : {
  "navbar" : {
    "Name" : "Naam",
    "Trancription" : "Omschrijving",
    "Special" : "Special",
    "MaxNum" : "Maximum aantal",
    "AFSD" : "Meerdaags arrangement",
    "TACO" : "dit arrangement beslaat",
    "nights" : "nachten",
    "Rebate" : "Kortingstype",
    "Type" : "Type",

    "voorjaar" : "voorjaar",
    "bosrijk" : "bosrijk",
    "zomer" : "zomer",
    "weekend" : "weekend",
    "zakelijk" : "zakelijk",
    "culinaire" : "culinaire",
    "feestelijk" : "feestelijk",
    "kindvriendelijk" : "kindvriendelijk",
    "natuur_en_sprot" : "nature and sports",
    "romantisch" : "romantisch",
    "verwen" : "verwen",
    "Park_Sleep_Fly" : "Park_Sleep_Fly",
    "Aan_het_water" : "Aan_het_water",

    "can" : "U kunt maximaal 3 types selecteren, houd de CTRL toets ingedrukt en klik de gewenste types aan.",
    "find" : "Mocht uw arrangements type er niet bij staan, geef het dan even door aan",
	
    "Note1" : "- Indien dit een meerdaags arrangement is, dan moet u het aantal nachten opgeven. De prijs die u hieronder opgeeft (*1) zal dan de totaalprijs zijn.",
    "Note2" : "- Indien u niet kiest voor een meerdaags arrangement, dan geldt de opgegeven prijs per overnachting (geen prijs per persoon invoeren). De meerprijs die u per kamer kunt opgeven geldt altijd per overnachting!",	
	
    "Rooms" : "Kamers",
    "StandardRoom" : "standaard kamer",
    "Surcharge" : "meerprijs per overnachting",
    "Avail" : "Beschikbaarheid",
    "Linked" : "De beschikbaarheid van het arrangement is gekoppeld aan de beschikbaarheid van de kamers",
	
    "SelectDates" : "Geldig Dates",

    "ValidOn" : "Tonen op",
    "Monday" : "maandag",
    "Tuesday" : "dinsdag",
    "Wednesday" : "woensdag",
    "Thursday" : "donderdag",
    "Friday" : "vrijdag",
    "Saturday" : "zaterdag",
    "Sunday" : "zondag",

    "LinkTo" : "Tonen op",
    "Dutdh" : "Nederlandse deel",
    "German" : "Duitse deel",
    "English" : "Engelse deel",
    "French" : "Franse deel",
	
    "request" : "Op aanvraag",

    "Image1" : "Foto 1",
    "Image2" : "Foto 2",

    "Add" : "Toevoegen",
    "Cancel1" : "Annuleren",
    "Save" : "Opslaan",
    "Cancel2" : "Annuleren",

    "Overview" : "Overview of the arrangement:",
	
    "Action" : "Acties",
    "Name" : "Naam",
    "Special" : "Special",
    "Reserved" : "Geboekt",
    "Status" : "Status",
    "request" : "Op aanvraag",
    "Link" : "Tonen op",
    "Delete" : "Delete",



    "new" : {
      "NEW" : "Neu",
      "PROJECT" : "Projekte",
      "TASK" : "Aufgabe",
      "USER" : "Benutzer",
      "EMAIL" : "E-Mail"
    },
    "NOTIFICATIONS" : "Benachrichtigungen"
  }
},
"aside" : {
  "nav" : {
    "HEADER" : "Navigation",
    "DASHBOARD" : "Armaturenbrett",
    "CALENDAR" : "Kalender",
    "EMAIL" : "E-Mail",
    "WIDGETS" : "Widgets",
    "components" : {
      "COMPONENTS" : "Komponenten",
      "ui_kits" : {
        "UI_KITS" : "UI Kits",
        "BUTTONS" : "Kn�pfe",
        "ICONS" : "Icons",
        "GRID" : "Grid",
        "BOOTSTRAP" : "Bootstrap",
        "SORTABLE" : "Sortable",
        "PORTLET" : "Portlet",
        "TIMELINE" : "Timeline",
        "VECTOR_MAP" : "Vektorkarte"
      },
      "table" : {
        "TABLE" : "Tabelle",
        "TABLE_STATIC" : "Tabelle statisch",
        "DATATABLE" : "Datatable",
        "FOOTABLE" : "Footable"
      },
      "form" : {
        "FORM" : "Form",
        "FORM_ELEMENTS" : "Formularelemente",
        "FORM_VALIDATION" : "Best�tigung",
        "FORM_WIZARD" : "Form wizard"
      },
      "CHART" : "Diagramm",
      "pages" : {
        "PAGES" : "Seiten",
        "PROFILE" : "Profil",
        "POST" : "Post",
        "SEARCH" : "Suchen",
        "INVOICE" : "Rechnung",
        "LOCK_SCREEN" : "Sperrbildschirm",
        "SIGNIN" : "Registrieren",
        "SIGNUP" : "Anmelden",
        "FORGOT_PASSWORD" : "Passwort vergessen",
        "404" : "404"
      }
    },
    "your_stuff" : {
      "YOUR_STUFF": "Ihr Material",
      "PROFILE" : "Profil",
      "DOCUMENTS" : "Dokumente"
    }
  },
  "MILESTONE" : "Meilenstein",
  "RELEASE" : "Freisetzung"
}

}
