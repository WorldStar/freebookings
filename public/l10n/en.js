{

"header" : {
  "navbar" : {
    "Name" : "Name",
    "Trancription" : "Trancription",
    "Special" : "Special",
    "MaxNum" : "Maximum number",
    "AFSD" : "Arrangements for serveral days",
    "TACO" : "this arrangement consists of",
    "nights" : "nights",
    "Rebate" : "Rebate System",
    "Type" : "Type",
	
    "voorjaar" : "spring",
    "bosrijk" : "bosrijk",
    "zomer" : "Summer",
    "weekend" : "weekend",
    "zakelijk" : "business",
    "culinaire" : "culinary",
    "feestelijk" : "festive",
    "kindvriendelijk" : "child-friendly",
    "natuur_en_sprot" : "nature and sports",
    "romantisch" : "romantic",
    "verwen" : "relax",
    "Park_Sleep_Fly" : "Park Sleep Fly",
    "Aan_het_water" : "Aan het water",

    "can" : "You can select max. 3 types, press the CTRL-key and select the desired type.",
    "find" : "If you don't find your type arrangment, please inform us",

    "Note1" : "- Please enter the number of nights, when the arrangment consists of serveral nights. The price below is the total price.",
    "Note2" : "- The price is per night, if you don't wish arrangment for serveral days(please enter no price per person). A surcharge is always per room, per night!",
	
    "Rooms" : "Rooms",
    "StandardRoom" : "Standard room",
    "Surcharge" : "Surcharge per night",
    "Avail" : "Availability",
    "Linked" : "Linked to the rooms",

    "SelectDates" : "Select Dates",
	
    "ValidOn" : "Valid on",
    "Monday" : "Monday",
    "Tuesday" : "Tuesday",
    "Wednesday" : "Wednesday",
    "Thursday" : "Thursday",
    "Friday" : "Friday",
    "Saturday" : "Saturday",
    "Sunday" : "Sunday",

    "LinkTo" : "Link to",
    "Dutdh" : "Dutdh",
    "German" : "German",
    "English" : "English",
    "French" : "French",

    "request" : "On request",
	
    "Image1" : "Image1",
    "Image2" : "Image2",

    "Add" : "Add",
    "Cancel1" : "Cancel",
    "Save" : "Save",
    "Cancel2" : "Cancel",

    "Overview" : "Overview of the arrangement:",

    "Action" : "Action",
    "Name" : "Action",
    "Special" : "Special",
    "Reserved" : "Reserved",
    "Status" : "Status",
    "request" : "On request",
    "Link" : "Link",
    "Delete" : "Delete",



	
    "new" : {
      "NEW" : "Neu",
      "PROJECT" : "Projekte",
      "TASK" : "Aufgabe",
      "USER" : "Benutzer",
      "EMAIL" : "E-Mail"
    },
    "NOTIFICATIONS" : "Benachrichtigungen"
  }
},
"aside" : {
  "nav" : {
    "HEADER" : "Navigation",
    "DASHBOARD" : "Armaturenbrett",
    "CALENDAR" : "Kalender",
    "EMAIL" : "E-Mail",
    "WIDGETS" : "Widgets",
    "components" : {
      "COMPONENTS" : "Komponenten",
      "ui_kits" : {
        "UI_KITS" : "UI Kits",
        "BUTTONS" : "Knöpfe",
        "ICONS" : "Icons",
        "GRID" : "Grid",
        "BOOTSTRAP" : "Bootstrap",
        "SORTABLE" : "Sortable",
        "PORTLET" : "Portlet",
        "TIMELINE" : "Timeline",
        "VECTOR_MAP" : "Vektorkarte"
      },
      "table" : {
        "TABLE" : "Tabelle",
        "TABLE_STATIC" : "Tabelle statisch",
        "DATATABLE" : "Datatable",
        "FOOTABLE" : "Footable"
      },
      "form" : {
        "FORM" : "Form",
        "FORM_ELEMENTS" : "Formularelemente",
        "FORM_VALIDATION" : "Bestätigung",
        "FORM_WIZARD" : "Form wizard"
      },
      "CHART" : "Diagramm",
      "pages" : {
        "PAGES" : "Seiten",
        "PROFILE" : "Profil",
        "POST" : "Post",
        "SEARCH" : "Suchen",
        "INVOICE" : "Rechnung",
        "LOCK_SCREEN" : "Sperrbildschirm",
        "SIGNIN" : "Registrieren",
        "SIGNUP" : "Anmelden",
        "FORGOT_PASSWORD" : "Passwort vergessen",
        "404" : "404"
      }
    },
    "your_stuff" : {
      "YOUR_STUFF": "Ihr Material",
      "PROFILE" : "Profil",
      "DOCUMENTS" : "Dokumente"
    }
  },
  "MILESTONE" : "Meilenstein",
  "RELEASE" : "Freisetzung"
}

}
