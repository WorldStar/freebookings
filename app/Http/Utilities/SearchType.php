<?php
/**
 * Created by PhpStorm.
 * User: Awais Muhammad
 * Date: 12-12-2015
 * Time: 12:06 PM
 */

namespace App\Http\Utilities;
use Illuminate\Foundation\Application;

class SearchType {

    protected static $lang;
    public function __construct(Application $app) {


        static::$lang = $app;

    }

    public static function get_search_type_list()
    {
        $list =[
            "en" => [
                "city"           => "City" ,
                "email"          => "E-mail",
                "password"       => "Password",
                "reference"      => "Reference",
            ],
            "nl" => [
                "city"           => "City" ,
                "email"          => "E-mail",
                "password"       => "Password",
                "reference"      => "Reference",
            ],
            "fr" => [
                "city"           => "City" ,
                "email"          => "E-mail",
                "password"       => "Password",
                "reference"      => "Reference",
            ],
            "de" => [
                "city"           => "City" ,
                "email"          => "E-mail",
                "password"       => "Password",
                "reference"      => "Reference",
            ]
        ];


        return $list[static::$lang->config->get("app.locale")];

    }
}