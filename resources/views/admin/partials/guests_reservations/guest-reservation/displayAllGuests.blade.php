<form  name="displayallguests" class="form-horizontal col-lg-10 col-lg-offset-1" style="" ng-submit=""
       ng-init="getAllGuests()">
    <div>
        <div  class="form-group">
            <label class="col-lg-2 control-label" >Display...guest</label>
            <div class="col-lg-4">
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox"  ng-model="checkboxModel.Nederland">
                        <span><img ng-src="..\img\flags\nl111.jpg" width="20px" tooltip="Nederland" tooltip-placement="bottom" >&nbsp; <span>Dutdh</span></span>
                    </label>
                </div>
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox" ng-model="checkboxModel.Deutsch">
                        <span><img ng-src="..\img\flags\du111.jpg" width="20px" tooltip="Deutsch" tooltip-placement="bottom" >&nbsp; <span>German</span></span>
                    </label>
                </div>
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox" ng-model="checkboxModel.English" >
                        <span><img ng-src="..\img\flags\en111.jpg" width="20px" tooltip="English" tooltip-placement="bottom" >&nbsp;<span>English</span></span>
                    </label>
                </div>
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox" ng-model="checkboxModel.Francais" >
                        <span><img ng-src="..\img\flags\fr111.jpg" width="20px" tooltip="Francais" tooltip-placement="bottom" >&nbsp; <span>French</span></span>
                    </label>
                </div>
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox" ng-model="checkboxModel.removedItem" >
                        <span>Display all removed items</span>
                    </label>
                </div>
                <div class="ui-checkbox ui-checkbox-info">
                    <label>
                        <input type="checkbox" ng-model="checkboxModel.businessGuest" >
                        <span>Only business guests</span>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        @inject('dayList','App\Http\Utilities\SearchType')
        <div>
            <label class="col-lg-2 control-label">Search type</label>
            <div class="btn-group col-lg-4">
                <div class="select-box">
                    <select id="priceDay" ng-model="searchForm.type" name="searchType" class="form-control">
                        <option value=""> Name </option>
                        @foreach($dayList::get_search_type_list() as $dayname => $dayval)
                            <option value="{{ $dayname }}"> {{ $dayval }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div>
            <label class="col-lg-2 control-label">From A to Z</label>
            <div class="btn-group col-lg-4">
                <div class="select-box">
                    <select id="priceDay" ng-model="searchForm.fromtoName" name="searchFromTo" class="form-control">
                        <option value=""> A to Z</option>
                        <option value="AtoM"> A to M</option>
                        <option value="NtoZ"> N to Z</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div>
            <label class="col-lg-2 control-label">Search content</label>
            <div class="btn-group col-lg-4">
                <input type="text" class="form-control input-sm" placeholder="Type keyword" data-ng-model="searchKeywords" data-ng-keyup="search()">
            </div>
        </div>
    </div>



    <p style="border-bottom:solid 1px rgb(230,230,230); width:100%" ></p>


    <div ng-show="showReservationNum">

        <div>
            <label class="col-lg-2 control-label"></label>
            <div>
                <b>Reservation from Lageveen, A.F., number = 0</b>
            </div>
        </div>
        <div>
            <label class="col-lg-2 control-label"></label>
            <div>
                <p>There are no reservation yet (or they are not displayed).</p>
            </div>
        </div>

        {{--<div>--}}
            {{--<label class="col-lg-2 control-label"></label>--}}
            {{--<div class="btn-group col-lg-4"  ng-click="information(item)">--}}
                {{--<img ng-src="..\img\icon\information.png"--}}
                     {{--width="20px"  tooltip="Reservation" tooltip-placement="bottom"--}}
                {{-->--}}
                {{--<span style="color:#ff9501"> Information about this guest </span>--}}
            {{--</div>--}}
            {{--<p style="border-bottom:solid 1px rgb(230,230,230); width:100%" ></p><br>--}}
        {{--</div>--}}

        <p style="border-bottom:solid 1px rgb(230,230,230); width:100%" ></p>


    </div>


    <div ng-show="showInfo">
        <b style="margin-left: 8%"> Guest Wang </b>
        <div class="form-group ">
            <label class="col-lg-2 control-label" ></label>
            <div class="col-lg-4" style=" margin-right:10px; cursor: pointer; cursor: hand;" ng-click="blackList(item)">
                <img ng-src="..\img\icon\blacklist.png"
                     width="20px"  tooltip="Reservation" tooltip-placement="bottom"
                     >
                <span style="color:#ff9501">&nbsp;Administer to black list </span>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-lg-2 control-label" ></label>
            <div class="col-lg-4" style=" margin-right:10px; cursor: pointer; cursor: hand;"
                 ng-click="reservationByThisGuest(item)">
                <img ng-src="..\img\icon\reservation.png"
                     width="21px" tooltip="Information" tooltip-placement="bottom"
                     style="margin-right:10px; cursor: pointer; cursor: hand;">
                <span style="color:#ff9501"> Reservation by this guest </span>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-lg-2 control-label" ></label>
            <div class="col-lg-4" style=" margin-right:10px; cursor: pointer; cursor: hand;" ng-click="sendEmail(item)">
                <img ng-src="..\img\icon\email.png"
                     width="20px" tooltip="Quick reservation" tooltip-placement="bottom"
                     style="margin-right:10px; cursor: pointer; cursor: hand;">
                <span style="color:#ff9501"> Send an email to this guest </span>
            </div>
        </div>
        <div class="form-group ">
            <label class="col-lg-2 control-label" ></label>
            <div class="col-lg-4" style=" margin-right:10px; cursor: pointer; cursor: hand;" ng-click="quickReservation(item)">
                <img  ng-src="..\img\icon\quick-reservation.png"
                     width="21px" tooltip="Email" tooltip-placement="bottom"
                     style="margin-right:10px;cursor: pointer; cursor: hand;">
                <span style="color:#ff9501"> Quick reservation </span>
            </div>
        </div>
        <div>
            <table class="infoTable">
                <tr >
                    <td style="width:20%">Reservation</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Accomerdations</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Company</td>
                    <td>No</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>dandong</td>
                </tr>
                <tr>
                    <td>ZIP</td>
                    <td>8606KC</td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>Sneek</td>
                </tr>
                <tr>
                    <td>Federal state</td>
                    <td>Frisland</td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td>China</td>
                </tr>
                <tr>
                    <td>Telephone</td>
                    <td>1884156987</td>
                </tr>
                <tr>
                    <td>E-mail</td>
                    <td>info@yahoo.com</td>
                </tr>
                <tr>
                    <td>Bank/Giro</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Account</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Number</td>
                    <td></td>
                </tr>
                <tr>
                    <td>Reasons from special guest</td>
                    <td>
                        <div class="col-lg-12" style="margin-left:-12px; margin-top:5px;">
                            <div ng-model="transcriptionReasons" class="text-angular-editor" text-angular></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Notes from the hotel</td>
                    <td>
                        <div class="col-lg-12" style="margin-left:-12px; margin-top:5px">
                            <div ng-model="transcriptionNotes" class="text-angular-editor" text-angular></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Type of relationship</td>
                    <td>
                        <div class="col-lg-12">
                            <input type="text" style="margin-left:-12px; margin-top:5px" name="relationship"
                               ng-model="relationship" class="form-control">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Book on account</td>
                    <td  class="col-lg-12" >
                        <div class="ui-checkbox ui-checkbox-info" style="margin-left:3px; margin-top:20px">
                            <label>
                                <input type="checkbox" ng-model="checkboxModel.account" >
                                <span>Yes, this invoice is 'On account'</span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-bottom: 10px">
                        <span>
                            <button  class="btn btn-primary" style="margin-left: 60%" type="submit">Change</button>
                        </span>
                        <span>
                            <button  class="btn btn-primary" type="submit">Delete</button>
                        </span>
                    </td>
                </tr>
            </table>
        </div><br><br>

        <b style="margin-left: 5%"> Discount for Wang </b>
        <div>
            <table class="infoTable" width="490px" style="float:left;margin-right:20px;">
                <tr >
                    <td >Status</td>
                    <td style="padding-top:5px; padding-right:5px">
                        <div class="select-box">
                            <select id="priceDay" ng-model="searchForm.fromtoName" name="searchFromTo" class="form-control">
                                <option value="">inactief</option>
                                <option value="AtoM">actief</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr >
                    <td >Room</td>
                    <td style="padding-top:5px">
                        <div class="ui-checkbox ui-checkbox-info">
                            <label>
                                <input type="checkbox" ng-model="checkboxModel.twin" >
                                <span>twin kamer</span>
                            </label>
                        </div>
                        <div class="ui-checkbox ui-checkbox-info">
                            <label>
                                <input type="checkbox" ng-model="checkboxModel.test" >
                                <span>test room</span>
                            </label>
                        </div>
                        <div class="ui-checkbox ui-checkbox-info">
                            <label>
                                <input type="checkbox" ng-model="checkboxModel.new" >
                                <span>new room</span>
                            </label>
                        </div>
                    </td>
                </tr>
                <tr >
                    <td>Percentage</td>
                    <td style="position:relative">
                        <input type="number" min="1" style="width:50%; float:left" name="relationship"
                                   ng-model="percentage" class="form-control">
                        <span style="position:absolute; margin-left:10px;margin-top:8px">%</span>
                        <span class="text-danger" ng-show="displayallguests.relationship.required">insert number of available rooms</span>
                        <span class="text-danger" ng-show="displayallguests.relationship.min">min 1 is required</span>
                    </td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td style="position:relative; padding-top:5px" >
                        <div class="col-lg-8">
                            <span class="fa fa-euro" style="position:absolute; margin-left:-15px; margin-top:8px"></span>
                            <input type="number" name="number" ng-model="newroom.number" min="1" class="form-control" integer-validity required>
                        </div>
                        <span class="text-danger" ng-show="displayallguests.number.required">insert number of available rooms</span>
                        <span class="text-danger" ng-show="displayallguests.number.min">min 1 is required</span>
                    </td>
                </tr>
                <tr>
                    <td>Provided that</td>
                    <td style="padding-top:5px; padding-right:5px">
                        <div class="select-box">
                            <select id="provided" ng-model="provideModel" name="provideThat" class="form-control">
                                <option value="">number of bookings</option>
                                <option value="AtoM">number of nights</option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr >
                    <td>You can view 2 example here</td>
                    <td></td>
                </tr>
                <tr >
                    <td>Number from</td>
                    <td style="padding-top:5px; padding-right:5px">
                        <div class="select-box">
                            <select ng-model="numberFrom"  class="form-control"
                                 ng-options="value for value in adjust_numbersFrom">
                            </select>
                        </div>

                    </td>
                </tr>
                <tr >
                    <td >Number to</td>
                    <td style="padding-top:5px; padding-right:5px">
                        <div class="select-box">
                            <select ng-model="numberTo"  class="form-control"
                                    ng-options="value for value in adjust_numbersTo">
                            </select>
                        </div>
                    </td>
                </tr>
                <tr >
                    <td>Arrival and/or depature from</td>
                    <td></td>
                </tr>
                <tr>
                    <td >Date from</td>
                    <td style=" padding-bottom:-20px; padding-right:5px">
                        <div ng-controller="DatepickerCtrl">
                            <div class="input-group">
                                <input type="text" class="form-control" ng-model="$parent.dateFrom" name="from"
                                    datepicker-popup is-open="fromOpened" min-date="minDate" max-date="maxDate"
                                    datepicker-options="dateOptions" date-disabled="disabled(date, mode)"
                                    ng-required="true" show-button-bar="false" placeholder="From">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-info fa fa-calendar"
                                         ng-click="fromOpen($event)"></button>
                                </span>
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td >Date to</td>
                    <td style="padding-right:5px">
                        <div ng-controller="DatepickerCtrl">
                            <div class="input-group mb0">
                                <input type="text" class="form-control" ng-model="$parent.dateUntil" name="to"
                                    datepicker-popup is-open="toOpened" min-date="minDate" max-date="maxDate"
                                    datepicker-options="dateOptions" date-disabled="disabled(date, mode)"
                                    ng-required="true" show-button-bar="false" placeholder="Until">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-sm btn-info fa fa-calendar"
                                         ng-click="toOpen($event)"></button>
                                </span>
                            </div>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-bottom: 10px; padding-top: 10px">
                        <span>
                            <button  class="btn btn-primary" style="margin-left: 30%" type="submit">Change</button>
                        </span>
                    </td>
                </tr>
            </table>

            <table class="infoTable" width="490px" >
                <tr>
                    <td><i>Example1</i></td><td></td>

                </tr>
                <tr>
                    <td colspan = "2" style="padding-right:10px">
                        You want to give the guest(who has never stayed over night in the hotel) a discount max.
                        once, provided that the guest will stay over night until the end of july.
                    </td>

                </tr>
                <tr >
                    <td>Provided that</td>
                    <td>Number of reservations</td>
                </tr>
                <tr>
                    <td>Number from </td>
                    <td >0</td>
                </tr>
                <tr>
                    <td>Number to </td>
                    <td >0</td>
                </tr>
                <tr >
                    <td>Date from</td>
                    <td>01-07-2016</td>
                </tr>
                <tr >
                    <td>Date to</td>
                    <td >01-08-2016</td>
                </tr>
                <tr style="height:23px">
                    <td colspan = "2" style="padding-right:20px;">
                        <p style="border-bottom:solid 1px rgb(230,230,230); width:100%; margin-top: 10px" ></p>
                    </td>

                </tr>
                <tr >
                    <td><i>Example2</i></td><td></td>
                </tr>
                <tr >
                    <td colspan = "2" style="padding-right:10px">
                        You want to give the guest(who has stayed 10 times over night in the hotel) a discount.
                        once, provided that the guest will stay over night until the end of july.
                    </td>

                </tr>
                <tr >
                    <td>Provided that</td>
                    <td>Number of accomodations</td>
                </tr>
                <tr>
                    <td>Number from </td>
                    <td >10</td>
                </tr>
                <tr>
                    <td>Number to </td>
                    <td >20+</td>
                </tr>
                <tr >
                    <td>Date from</td>
                    <td>02-07-2016</td>
                </tr>
                <tr >
                    <td>Date to</td>
                    <td >01-08-2016</td>
                </tr>
            </table>
        </div>

        <p style="border-bottom:solid 1px rgb(230,230,230); width:100%; margin-top: 10px" ></p>

    </div>


    <div ng-show="showEmail">
        <table class="infoTable">
            <tr >
                <td style="width:20%">Subject</td>
                <td>
                    <div class="col-lg-12">
                        <input type="text" style="margin-left:-12px; margin-top:5px" name="relationship"
                               ng-model="subject" class="form-control">
                    </div>
                </td>
            </tr>
            <tr>
                <td>Title</td>
                <td>
                    <div class="col-lg-12">
                        <input type="text" style="margin-left:-12px; margin-top:5px" name="relationship"
                               ng-model="title" class="form-control">
                    </div>
                </td>
            </tr>
            <tr>
                <td>Message</td>
                <td>
                    <div class="col-lg-12" style="margin-left:-12px; margin-top:5px;">
                        <div ng-model="message" class="text-angular-editor" text-angular></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>Signature</td>
                <td>
                    <div class="col-lg-12" style="margin-left:-12px; margin-top:5px">
                        <div ng-model="signature" class="text-angular-editor" text-angular></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="padding-bottom:10px; padding-top:10px">
                    <span>
                        <button  class="btn btn-primary" style="margin-left: 60%" type="submit">Send</button>
                    </span>
                    <span>
                        <button  class="btn btn-primary" type="submit">Cancel/back</button>
                    </span>
                </td>
            </tr>
        </table>
        <p style="border-bottom:solid 1px rgb(230,230,230); width:100%; margin-top: 10px" ></p>

    </div>



    <div ng-show="showBlackList">
        <label class="col-lg-2 control-label" ><b>Put a person on the blank list.</b></label>

        <div>
            <table class="infoTable">
                <tr >
                    <td style="width:20%">Name</td>
                    <td>
                        <div class="col-lg-12">
                            <input type="text" style="margin-left:-12px; margin-top:5px" name="relationship"
                                   ng-model="blackName" class="form-control">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>
                        <div class="col-lg-12" style="margin-left:-12px; margin-top:5px;">
                            <div ng-model="blankDescription" class="text-angular-editor" text-angular></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-bottom:10px; padding-top:10px">
                        <span>
                            <button  class="btn btn-primary" style="margin-left: 60%" type="submit">Send</button>
                        </span>
                        <span>
                            <button  class="btn btn-primary" type="submit">Cancel/back</button>
                        </span>
                    </td>
                </tr>
            </table>
            <p style="border-bottom:solid 1px rgb(230,230,230); width:100%; margin-top: 10px" ></p>

        </div>

        <label class="col-lg-2 control-label" ><b>Blank list.</b></label>
        <div style="margin-left: 17%">
            <table width="96%">

                <thead>
                <tr>
                    <td width="20%" style="text-align:center">
                        <div class="div_changeprice" >Name</div>
                    </td>
                    <td style="text-align:center">
                        <div class="div_changeprice" >Desciption</div>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="item in guestsData" height="30px" style="margin:3px" >
                    <td class="div_changeprice" style="text-align:center">@{{item.name}}</td>
                    <td class="div_changeprice" style="text-align:center">@{{item.description}}</td>
                </tr>
                </tbody>

            </table>
        </div>
        <p style="border-bottom:solid 1px rgb(230,230,230); width:100%; margin-top: 10px" ></p>

    </div>




    <div class="col-lg-12"  ng-show="showTable">
        <b> All guests, displayed are 20 of 100 = 3.  </b>
        <p>You have the possibility to mark your guests as 'special guest' and to send the mailing to him for
            example.<br> Click on information, if you searched information about the guest, discount regulations, or if
            you wanted to add guest to the blank list.</p><br>

        <table >

            <thead>
            <tr>
                <td width="15%" style="text-align:center">
                    <div class="div_changeprice">Action</div>
                </td>
                <td width="10%" style="text-align:center">
                    <div class="div_changeprice" >Name</div>
                </td>
                <td width="10%" style="text-align:center">
                    <div class="div_changeprice" >Gender</div>
                </td>
                <td width="10%" style="text-align:center">
                    <div class="div_changeprice" >Country</div>
                </td>
                <td width="15%" style="text-align:center">
                    <div class="div_changeprice">E-mail</div>
                </td>
                <td width="15%" style="text-align:center">
                    <div class="div_changeprice">Address</div>
                </td>
                <td width="5%" style="text-align:center">
                    <div class="div_changeprice">Special</div>
                </td>
                <td width="5%" style="text-align:center">
                    <div class="div_changeprice">business</div>
                </td>
                <td width="10%" style="text-align:center">
                    <div class="div_changeprice">Benefit</div>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="item in guestsData" height="30px" style="margin:3px" >
                <td  class="div_changeprice" style="text-align:center">
                    <img ng-click="reservation(item)" ng-src="..\img\icon\reservation.png"
                         width="24px"  tooltip="Reservation" tooltip-placement="bottom"
                         style="margin-left:10px; margin-right:10px; cursor: pointer; cursor: hand;">
                    <img ng-click="information(item)" ng-src="..\img\icon\information.png"
                         width="21px" tooltip="Information" tooltip-placement="bottom"
                         style="margin-right:10px; cursor: pointer; cursor: hand;">
                    <img ng-click="quickReservation(item)" ng-src="..\img\icon\quick-reservation.png"
                         width="22px" tooltip="Quick reservation" tooltip-placement="bottom"
                         style="margin-right:10px; cursor: pointer; cursor: hand;">
                    <img ng-click="emailen(item)" ng-src="..\img\icon\email.png"
                         width="25px" tooltip="Email" tooltip-placement="bottom"
                         style="margin-right:10px;cursor: pointer; cursor: hand;">
                </td>
                <td class="div_changeprice" style="text-align:center">@{{item.name}}</td>
                <td class="div_changeprice" style="text-align:center">@{{item.gender}}</td>
                <td class="div_changeprice" style="text-align:center">@{{item.country}}</td>
                <td class="div_changeprice" style="text-align:center">@{{item.email}}</td>
                <td class="div_changeprice" style="text-align:center">@{{item.address}}</td>
                <td class="div_changeprice" style="text-align:center">
                    <input type="checkbox" ng-model="item.special" >
                </td>
                <td class="div_changeprice" style="text-align:center">
                    <input type="checkbox" ng-model="item.business" >
                </td>
                <td class="div_changeprice" style="text-align:center; cursor: pointer; cursor: hand;"
                    ng-click="gotoNewPage(item.name)">
                    @{{item.benefit}}
                </td>
            </tr>
            </tbody>

        </table>
        <button  class="btn btn-primary" type="submit" style="margin-top:20px; margin-left:81%">Save</button>

    </div>
</form>

