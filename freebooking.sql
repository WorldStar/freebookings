/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.9-MariaDB : Database - freebooking
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`freebooking` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `freebooking`;

/*Table structure for table `administer_arrangements` */

DROP TABLE IF EXISTS `administer_arrangements`;

CREATE TABLE `administer_arrangements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` int(10) unsigned NOT NULL,
  `arrangement_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `available` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `administer_arrangements_hotel_id_foreign` (`hotel_id`),
  KEY `administer_arrangements_arrangement_id_foreign` (`arrangement_id`),
  CONSTRAINT `administer_arrangements_arrangement_id_foreign` FOREIGN KEY (`arrangement_id`) REFERENCES `arrangements` (`id`),
  CONSTRAINT `administer_arrangements_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `administer_arrangements` */

/*Table structure for table `arrangement_descriptions` */

DROP TABLE IF EXISTS `arrangement_descriptions`;

CREATE TABLE `arrangement_descriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `options` text COLLATE utf8_unicode_ci NOT NULL,
  `options_text` text COLLATE utf8_unicode_ci NOT NULL,
  `options_text_checked` text COLLATE utf8_unicode_ci NOT NULL,
  `language` enum('nl','en','de','fr') COLLATE utf8_unicode_ci NOT NULL,
  `arrangement_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `arrangement_descriptions_arrangement_id_foreign` (`arrangement_id`),
  KEY `arrangement_descriptions_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `arrangement_descriptions_arrangement_id_foreign` FOREIGN KEY (`arrangement_id`) REFERENCES `arrangements` (`id`),
  CONSTRAINT `arrangement_descriptions_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `arrangement_descriptions` */

insert  into `arrangement_descriptions`(`id`,`description`,`options`,`options_text`,`options_text_checked`,`language`,`arrangement_id`,`hotel_id`,`created_at`,`updated_at`) values (185,'','','','','en',47,2,'2016-07-19 08:28:13','2016-07-19 08:33:51'),(186,'jhon is good.','','','','fr',47,2,'2016-07-19 08:28:13','2016-07-19 08:28:13'),(187,'jhon is good.','','','','nl',47,2,'2016-07-19 08:28:13','2016-07-19 08:28:13'),(188,'jhon is good.','','','','de',47,2,'2016-07-19 08:28:13','2016-07-19 08:28:13'),(189,'halm is not bad','','','','en',48,2,'2016-07-19 09:03:58','2016-07-19 09:03:58'),(190,'halm is not bad','','','','fr',48,2,'2016-07-19 09:03:58','2016-07-19 09:03:58'),(191,'halm is not bad','','','','nl',48,2,'2016-07-19 09:03:58','2016-07-19 09:03:58'),(192,'halm is not bad','','','','de',48,2,'2016-07-19 09:03:58','2016-07-19 09:03:58');

/*Table structure for table `arrangement_price_management` */

DROP TABLE IF EXISTS `arrangement_price_management`;

CREATE TABLE `arrangement_price_management` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `maximum_available` tinyint(4) NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `arrangement_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `arrangement_price_management_arrangement_id_foreign` (`arrangement_id`),
  KEY `arrangement_price_management_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `arrangement_price_management_arrangement_id_foreign` FOREIGN KEY (`arrangement_id`) REFERENCES `arrangements` (`id`),
  CONSTRAINT `arrangement_price_management_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `arrangement_price_management` */

/*Table structure for table `arrangements` */

DROP TABLE IF EXISTS `arrangements`;

CREATE TABLE `arrangements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rebate` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `rooms` text COLLATE utf8_unicode_ci NOT NULL,
  `special` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `persons` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `patroon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `standard_room` mediumint(9) NOT NULL,
  `price_from` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `maximum_available` tinyint(4) NOT NULL,
  `on_request` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `more_days` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `nights` tinyint(4) NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `discount_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `position` tinyint(4) NOT NULL,
  `linked_rooms_available` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `extra_price_with_room_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `arrangements_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `arrangements_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `arrangements` */

insert  into `arrangements`(`id`,`name`,`description`,`rebate`,`rooms`,`special`,`persons`,`price`,`status`,`date_from`,`date_to`,`patroon`,`standard_room`,`price_from`,`maximum_available`,`on_request`,`language`,`more_days`,`nights`,`type`,`discount_type`,`position`,`linked_rooms_available`,`extra_price_with_room_price`,`hotel_id`,`created_at`,`updated_at`) values (47,'jhon','','0','1||','1','','||','1','2016-07-20','2016-07-21','Sun',0,'11',1,'1','Ne','1',1,'voorjaar','1',0,'0','',2,'2016-07-19 08:28:13','2016-07-19 08:33:51'),(48,'Halm','halm is not bad','1','1|','1','','22|33|','1','2016-07-21','2016-07-22','Tue',0,'',1,'1','De','0',3,'feestelijk','0',0,'0','',2,'2016-07-19 09:03:58','2016-07-19 09:03:58');

/*Table structure for table `guests` */

DROP TABLE IF EXISTS `guests`;

CREATE TABLE `guests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hotel_id` int(10) unsigned NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `guests_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `guests_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `guests` */

/*Table structure for table `hotel_extra_services` */

DROP TABLE IF EXISTS `hotel_extra_services`;

CREATE TABLE `hotel_extra_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `hotel_extra_services` */

/*Table structure for table `hotel_options` */

DROP TABLE IF EXISTS `hotel_options`;

CREATE TABLE `hotel_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_option` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_options_title` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `hotel_options_user_id_foreign` (`user_id`),
  CONSTRAINT `hotel_options_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `hotel_options` */

/*Table structure for table `hotel_settings` */

DROP TABLE IF EXISTS `hotel_settings`;

CREATE TABLE `hotel_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_excluding` enum('excluding','including') COLLATE utf8_unicode_ci NOT NULL,
  `creditcards` text COLLATE utf8_unicode_ci NOT NULL,
  `creditcards_text_checked` text COLLATE utf8_unicode_ci NOT NULL,
  `creditcards_text` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_option_text_checked` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_option_text` text COLLATE utf8_unicode_ci NOT NULL,
  `reserve_without_credit_card` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `booking_without_credit_card_email` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `guest_check_in_form` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `tourist_tax_checkbox` enum('false','true') COLLATE utf8_unicode_ci NOT NULL,
  `tax_amt` enum('per room','per person','percentage') COLLATE utf8_unicode_ci NOT NULL,
  `tax_amt_val` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_by_freebookings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reservations_by_freebookings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_rooms_freebookings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `guest_arrival` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `engine_page_display` enum('hotel_info','arrangement','kamer','hotel_review') COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `hotel_settings_user_id_foreign` (`user_id`),
  CONSTRAINT `hotel_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `hotel_settings` */

/*Table structure for table `hotel_texts` */

DROP TABLE IF EXISTS `hotel_texts`;

CREATE TABLE `hotel_texts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `short_description` text COLLATE utf8_unicode_ci NOT NULL,
  `long_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `hotel_locations` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_title_1` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `custom_description_1` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_title_2` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `custom_description_2` text COLLATE utf8_unicode_ci NOT NULL,
  `event` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `event_description` text COLLATE utf8_unicode_ci NOT NULL,
  `language` enum('nl','en','fr','de') COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `hotel_texts_user_id_foreign` (`user_id`),
  CONSTRAINT `hotel_texts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `hotel_texts` */

/*Table structure for table `hotels` */

DROP TABLE IF EXISTS `hotels`;

CREATE TABLE `hotels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `check_in` time NOT NULL,
  `check_out` time NOT NULL,
  `overStar` enum('1','2','3','4','5') COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `giro_account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `iban_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `swift_ibc` enum('swift','ibc','3','4','5') COLLATE utf8_unicode_ci NOT NULL,
  `swift_ibc_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ihk_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tax_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `hotels_user_id_foreign` (`user_id`),
  CONSTRAINT `hotels_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `hotels` */

insert  into `hotels`(`id`,`address`,`postcode`,`country`,`state`,`city`,`phone`,`fax`,`check_in`,`check_out`,`overStar`,`account_number`,`giro_account_number`,`iban_number`,`swift_ibc`,`swift_ibc_number`,`ihk_number`,`tax_number`,`user_id`,`created_at`,`updated_at`) values (1,'','','','','','','','00:00:00','00:00:00','1','','','','swift','','','',1,'2016-07-18 08:16:21','2016-07-18 08:16:21'),(2,'','','','','','','','00:00:00','00:00:00','1','','','','swift','','','',2,'2016-07-18 08:16:21','2016-07-18 08:16:21');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_100000_create_password_resets_table',1),('2015_11_10_121336_create_users_table',1),('2015_11_10_125851_create_hotel_texts_table',1),('2015_11_10_125851_create_hotels_table',1),('2015_11_27_121659_create_hotel_settings_table',1),('2015_12_02_092722_create_hotel_options_table',1),('2015_12_03_084556_create_rooms_table',1),('2015_12_09_071309_create_room_descriptions_table',1),('2015_12_11_074019_create_room_options_table',1),('2015_12_14_102331_create_room_price_info_table',1),('2016_01_01_083302_create_room_photos',1),('2016_05_12_062648_create_arrangements_table',1),('2016_05_14_203309_create_arrangement_descriptions_table',1),('2016_05_14_203808_create_arrangement_price_management_table',1),('2016_06_02_091230_guests',1),('2016_06_02_092737_reservation_options',1),('2016_06_02_094234_reservation',1),('2016_06_02_095753_reservation_extras',1),('2016_06_02_101635_reservation_payment',1),('2016_06_06_133927_hotel_extra_services',1),('2016_06_23_070731_create_arrangement_administer_availability',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `reservation` */

DROP TABLE IF EXISTS `reservation`;

CREATE TABLE `reservation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guest_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `checkin` datetime NOT NULL,
  `checkout` datetime NOT NULL,
  `arrangement_id` int(10) unsigned NOT NULL,
  `num_of_rooms` tinyint(4) NOT NULL,
  `num_of_persons` tinyint(4) NOT NULL,
  `arrival_time` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `reservation_guest_id_foreign` (`guest_id`),
  KEY `reservation_hotel_id_foreign` (`hotel_id`),
  KEY `reservation_room_id_foreign` (`room_id`),
  KEY `reservation_arrangement_id_foreign` (`arrangement_id`),
  CONSTRAINT `reservation_arrangement_id_foreign` FOREIGN KEY (`arrangement_id`) REFERENCES `arrangements` (`id`),
  CONSTRAINT `reservation_guest_id_foreign` FOREIGN KEY (`guest_id`) REFERENCES `guests` (`id`),
  CONSTRAINT `reservation_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `reservation_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reservation` */

/*Table structure for table `reservation_extras` */

DROP TABLE IF EXISTS `reservation_extras`;

CREATE TABLE `reservation_extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `reservation_extras_reservation_id_foreign` (`reservation_id`),
  CONSTRAINT `reservation_extras_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reservation_extras` */

/*Table structure for table `reservation_options` */

DROP TABLE IF EXISTS `reservation_options`;

CREATE TABLE `reservation_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guest_id` int(10) unsigned NOT NULL,
  `book_via` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `send_booking_mail` tinyint(1) NOT NULL,
  `send_booking_fax` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `reservation_options_guest_id_foreign` (`guest_id`),
  CONSTRAINT `reservation_options_guest_id_foreign` FOREIGN KEY (`guest_id`) REFERENCES `guests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reservation_options` */

/*Table structure for table `reservation_payment` */

DROP TABLE IF EXISTS `reservation_payment`;

CREATE TABLE `reservation_payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `option` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cc_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cc_num` bigint(20) NOT NULL,
  `cc_type` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cvv` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `reservation_payment_reservation_id_foreign` (`reservation_id`),
  CONSTRAINT `reservation_payment_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `reservation_payment` */

/*Table structure for table `room_descriptions` */

DROP TABLE IF EXISTS `room_descriptions`;

CREATE TABLE `room_descriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `language` enum('nl','en','de','fr') COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `room_descriptions_room_id_foreign` (`room_id`),
  KEY `room_descriptions_user_id_foreign` (`user_id`),
  KEY `room_descriptions_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `room_descriptions_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `room_descriptions_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_descriptions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_descriptions` */

insert  into `room_descriptions`(`id`,`description`,`language`,`room_id`,`user_id`,`hotel_id`,`created_at`,`updated_at`) values (1,'','en',1,1,1,'2016-07-18 16:22:38','2016-07-18 16:22:38'),(2,'','fr',1,1,1,'2016-07-18 16:22:38','2016-07-18 16:22:38'),(3,'','nl',1,1,1,'2016-07-18 16:22:38','2016-07-18 16:22:38'),(4,'','de',1,1,1,'2016-07-18 16:22:38','2016-07-18 16:22:38');

/*Table structure for table `room_options` */

DROP TABLE IF EXISTS `room_options`;

CREATE TABLE `room_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custom_option` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_options_title` text COLLATE utf8_unicode_ci NOT NULL,
  `option` text COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `room_options_room_id_foreign` (`room_id`),
  KEY `room_options_user_id_foreign` (`user_id`),
  KEY `room_options_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `room_options_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `room_options_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_options_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_options` */

/*Table structure for table `room_photos` */

DROP TABLE IF EXISTS `room_photos`;

CREATE TABLE `room_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `room_photos_room_id_foreign` (`room_id`),
  KEY `room_photos_user_id_foreign` (`user_id`),
  KEY `room_photos_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `room_photos_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `room_photos_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_photos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_photos` */

/*Table structure for table `room_price_info` */

DROP TABLE IF EXISTS `room_price_info`;

CREATE TABLE `room_price_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `status` enum('','v','b') COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `last_minute` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `lm_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `available` tinyint(4) NOT NULL,
  `leased` tinyint(4) NOT NULL,
  `lm_ref_id` mediumint(9) NOT NULL,
  `lm_patroon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ms_ref_id` mediumint(9) NOT NULL,
  `ms_patroon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ms_nights` tinyint(4) NOT NULL,
  `ms_on_request` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `minimal_stay` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `discount` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `discount_percentage` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `discount_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `discount_start` tinyint(4) NOT NULL,
  `discount_end` tinyint(4) NOT NULL,
  `discount_patroon` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `discount_ref_id` mediumint(9) NOT NULL,
  `discount_compare` enum('dailyprice','totalprice') COLLATE utf8_unicode_ci NOT NULL,
  `discount_type` enum('','earlybooking') COLLATE utf8_unicode_ci NOT NULL,
  `ex_ref_id` mediumint(9) NOT NULL,
  `ex_breakfast` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `ex_breakfast_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `single_use_ref_id` mediumint(9) NOT NULL,
  `single_use` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `single_use_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `non_refundable_ref_id` mediumint(9) NOT NULL,
  `nonrefundable` enum('n','j') COLLATE utf8_unicode_ci NOT NULL,
  `nonrefundable_price` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `room_price_info_room_id_foreign` (`room_id`),
  KEY `room_price_info_user_id_foreign` (`user_id`),
  KEY `room_price_info_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `room_price_info_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `room_price_info_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `room_price_info_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `room_price_info` */

insert  into `room_price_info`(`id`,`date`,`status`,`price`,`last_minute`,`lm_price`,`available`,`leased`,`lm_ref_id`,`lm_patroon`,`ms_ref_id`,`ms_patroon`,`ms_nights`,`ms_on_request`,`minimal_stay`,`discount`,`discount_percentage`,`discount_price`,`discount_start`,`discount_end`,`discount_patroon`,`discount_ref_id`,`discount_compare`,`discount_type`,`ex_ref_id`,`ex_breakfast`,`ex_breakfast_price`,`single_use_ref_id`,`single_use`,`single_use_price`,`non_refundable_ref_id`,`nonrefundable`,`nonrefundable_price`,`room_id`,`user_id`,`hotel_id`,`created_at`,`updated_at`) values (1,'2016-07-19','','200','j','200',1,0,1,'ma-di-wo-do',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 10:17:42'),(2,'2016-07-20','','200','j','200',1,0,1,'ma-di-wo-do',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 10:17:42'),(3,'2016-07-21','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(4,'2016-07-22','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(5,'2016-07-23','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(6,'2016-07-24','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(7,'2016-07-25','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(8,'2016-07-26','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(9,'2016-07-27','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(10,'2016-07-28','','200','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:54:37','2016-07-19 03:54:37'),(11,'2016-07-13','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(12,'2016-07-14','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(13,'2016-07-15','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(14,'2016-07-16','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(15,'2016-07-17','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(16,'2016-07-18','','40','n','',1,0,0,'',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 03:55:46','2016-07-19 03:55:46'),(17,'2016-07-19','','','n','200',0,0,17,'ma-di-wo-do-vr-za-zo',0,'',0,'n','n','n','','',0,0,'',0,'dailyprice','',0,'n','',0,'n','',0,'n','',1,1,1,'2016-07-19 10:15:16','2016-07-19 10:17:42');

/*Table structure for table `rooms` */

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `number_persons` int(11) NOT NULL,
  `price` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `special_room` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `special_text` text COLLATE utf8_unicode_ci NOT NULL,
  `single_use` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `single_use_discount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ex_breakfast` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `ex_breakfast_discount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `hotel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `rooms_user_id_foreign` (`user_id`),
  KEY `rooms_hotel_id_foreign` (`hotel_id`),
  CONSTRAINT `rooms_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `rooms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `rooms` */

insert  into `rooms`(`id`,`name`,`number`,`number_persons`,`price`,`status`,`special_room`,`special_text`,`single_use`,`single_use_discount`,`ex_breakfast`,`ex_breakfast_discount`,`user_id`,`hotel_id`,`created_at`,`updated_at`) values (1,'jhon',1,1,'40','1','1','','1','','1','',1,1,'2016-07-18 16:22:38','2016-07-18 16:22:38');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referral` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `initials` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `website_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_type` enum('hotel','Bed & Breakfast') COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('hotel','admin') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','deactive','deleted') COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `gender` enum('m','f') COLLATE utf8_unicode_ci NOT NULL,
  `language` enum('nl','en','fr','du') COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`referral`,`initials`,`first_name`,`last_name`,`hotel_name`,`email`,`profile_picture`,`website_url`,`address`,`postcode`,`city`,`state`,`country`,`phone`,`fax`,`password`,`hotel_type`,`type`,`status`,`birth_date`,`gender`,`language`,`remember_token`,`created_at`,`updated_at`) values (1,'','','Shams','Hashmi','Hotel Hogerhuys','shams@freebookings.nl','','','','','','','','','','$2y$10$KIEfjofILWbgxx..H8XAn.Toj59pvF9anJztKW5/NqwrEFalPrtzy','hotel','admin','active','0000-00-00','','nl',NULL,'2016-07-18 08:16:21','2016-07-18 08:16:21'),(2,'','','Aaldert','Lageveen','Hotel Hogerhuys','info@freebookings.nl','','','','','','','','','','$2y$10$JT0Yf7ZOqRdFD2KFByzXxeZevK3pd1JLhLVIiUdPnwg2BL37zVP5O','hotel','hotel','active','0000-00-00','','nl',NULL,'2016-07-18 08:16:21','2016-07-18 08:16:21');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
