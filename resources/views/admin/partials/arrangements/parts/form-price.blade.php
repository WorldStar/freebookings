
<form  name="newArrangeForm1" class="form-horizontal col-lg-10 col-lg-offset-1" ng-submit="arrangemtResult()" ng-show="!showRoom" Style="float:none">
	<p>- Please enter the number of nights, when the arrangment consists of serveral nights. 
		The price below is the total price.</p>
	<p>- The price is per night, if you don't wish arrangment for serveral days(please enter no price per person).
		A surcharge is always per room, per night! </p><br>

	<p style="border-bottom:solid 1px rgb(230,230,230); width:90%; margin-top:-10px"></p>

	<div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-8" style="margin-top:7px">
			<p>Prijs koppelen aan kamerprijs</p>
        </div>
	</div>
	<div class="form-group">
        <label class="col-lg-2 control-label pt0"></label>
        <div class="col-lg-8">
            <div class="ui-radio ui-radio-primary mb0" ng-repeat="priceChoice in priceQuestions">
			
				<label class="ui-radio-inline">
					<input type="radio" name="pricePerRoom" ng-model="$parent.priceroom" ng-value="priceChoice" ng-click="setPriceQuestion(priceChoice)"/>
					<span style="margin-top:10px">@{{priceChoice.priceLabel}}&nbsp; 
						<span class="fa fa-euro"></span>
						<input type="text" ng-model="priceChoice.editPrice" min="1" name="editPrice@{{$index}}" numeric-validity required>&nbsp; @{{priceChoice.endLabel}}
					</span>
				</label>
				<div class="col-lg-8 col-lg-push-2">
					<span class="text-danger" ng-show="newArrangeForm1['editPrice'+$index].required">insert number of available rooms</span>
					<span class="text-danger" ng-show="newArrangeForm1['editPrice'+$index].min">min 1 is required</span>
				</div>

			</div>
        </div>
    </div>

	<p style="color:#ff271e">
		Om prijzen aan te passen die u bij de opmark van arrangement heeft aangemaakt, die u naar de module: 
		"prijs en beschikbaarheid" of "Prijzen beheren" te gaan! <br>Alleen daar worden prijs aanpassingen doorgevoerd in het systeem. 
	</p>
	<p style="border-bottom:solid 1px rgb(230,230,230); width:90%; margin-top:20px; "></p>
	
	
	<div class="form-group">
		<label class="col-lg-2 control-label">Rooms</label>
        <div class="col-lg-8" style="margin-top:7px; float:left; width:20%">
			<p></p>
        </div>
        <div class="col-lg-8" style="margin-top:7px; float:left; width:20%">
			<p>Standard room</p>
        </div>
        <div class="col-lg-8" style="margin-top:7px; width:20%">
			<p>Surcharge per night</p>
        </div>
	</div>

	<div class="form-group">
		<label class="col-lg-2 control-label"></label>
        <div class="col-lg-8" style="float:left; width:20%">
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.twin">
					<span>twin-kamer</span>
				</label>
			</div>

			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.test">
					<span>test room</span>
				</label>
			</div>
        </div>
        <div class="col-lg-8" style="float:left; width:20%">
			<div class="ui-radio ui-radio-primary mb0">
			
				<label class="ui-radio-inline" ng-repeat="choiceRoom in standardQuestions" style="margin-top:5px">
					<input type="radio" ng-value="choiceRoom" ng-model="$parent.choiceOfStandardRoom" ng-click="setStandardQuestion(choiceRoom)">
					<span>@{{choiceRoom.label}}</span>
				</label>
				
			</div>
        </div>
		
        <div class="col-lg-8" style="width:40%" ng-show="showHide1">
			<div style="margin-top:-3px">
				<span class="fa fa-euro"></span>
				<input type="text" ng-model="price1" min="1" name="Price1" numeric-validity required>
				<span class="text-warning" ng-show="newArrangeForm1.Price1.$error.numeric">Insert valid number</span>
				<span class="text-warning" ng-show="newArrangeForm1.Price1.required">insert price</span>
			</div >
			<div style="margin-top:3px">
				<span class="fa fa-euro"></span>
				<input type="text" ng-model="price2" min="1" name="Price2" numeric-validity required>
				<span class="text-warning" ng-show="newArrangeForm1.Price2.$error.numeric">Insert valid number</span>
				<span class="text-warning" ng-show="newArrangeForm1.Price2.required">insert price</span>
			</div>
        </div>
		
	</div>
		
	<div class="form-group">
		<label class="col-lg-2 control-label">Availability</label>
		<div class="col-lg-8" style="margin-top:7px">
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.availability" ng-checked="flag003">
					<span>Linked to the rooms</span>
				</label>
			</div>
		</div>
	</div>	


</form>

