<form  name="newArrangeForm3" class="form-horizontal col-lg-10 col-lg-offset-1" ng-submit="arrangemtResult()" ng-show="!showRoom">
	<div class="form-group">
		<label class="col-lg-4 control-label" >Image1</label>
		<div class="col-lg-8" style="margin-top:5px">
			<input type="file"  name="upload_picture1" ng-model="upload_picture1" title="add upload picutre">
		</div>
    </div>
    <div class="form-group">
		<label class="col-lg-4 control-label" >Image2</label>
		<div class="col-lg-8" style="margin-top:5px">
			<input type="file"  name="upload_picture2" ng-model="upload_picture2" title="add upload picutre">
		</div>
    </div><br><br><br>

	<div class=" text-center">
		<div class="btn-group" ng-show="showAdd">
		
			<button  class="btn btn-primary" type="submit" style="margin-right:20px" ng-click="Add()" >Add</button>
			<button  class="btn btn-primary" type="submit" ng-click="Cancel1()" >Cancel</button>
		</div>
		<div class="btn-group" ng-show="showSave">
			<button  class="btn btn-primary" type="submit" style="margin-right:20px" ng-click="Save()" >Save</button>
			<button  class="btn btn-primary" type="submit" ng-click="Cancel2()">Cancel</button>
		</div>
	</div><br><br><br>
	
	<head>
		<title>Angular JS Table</title>
		  
		<style>
			table, th , td {
				border: 1px solid grey;
				border-collapse: collapse;
			}
		</style>
		  
	</head>	

	<b>Overview of the arrangement</b><br><br>

	<div class="col-lg-12">

		<table width="100%">

			<thead>
				<tr>
					<td width="20%" style="text-align:center">
						<div class="div_changeprice">Action</div>
					</td>
					<td width="20%" style="text-align:center">
						<div class="div_changeprice" >Name</div>
					</td>
					<td width="10%" style="text-align:center">
						<div class="div_changeprice" >Special</div>
					</td>
					<td width="10%" style="text-align:center">
						<div class="div_changeprice" >Reserved</div>
					</td>
					<td width="10%" style="text-align:center">
						<div class="div_changeprice">Status</div>
					</td>
					<td width="10%" style="text-align:center">
						<div class="div_changeprice">On request</div>
					</td>
					<td width="10%" style="text-align:center">
						<div class="div_changeprice">Link to</div>
					</td>
					<td width="5%" style="text-align:center">
						<div class="div_changeprice">Delete</div>
					</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="item in arrangementData" style="margin:3px">
					<td class="div_changeprice" style="text-align:center">
						<img ng-click="changeArrangement(item.name)" ng-src="..\img\icon\Setting.png" width="15px" tooltip="Change arrangement" tooltip-placement="bottom" style="margin-right:20px; cursor: pointer; cursor: hand;">
						<img ng-click="adminAvailibility(item)" ng-src="..\img\icon\Calendar.png" width="14px" tooltip="Administer abailability" tooltip-placement="bottom"  style="margin-right:20px; cursor: pointer; cursor: hand;">
						<img ng-click="adminPrices(item)" ng-src="..\img\icon\Euro.png" width="12px" tooltip="Administer prices" tooltip-placement="bottom" style="margin-right:20px; cursor: pointer; cursor: hand;">
						<img ng-click="adminPriceAvalability(item)" ng-src="..\img\icon\UeroCalendar.png" width="18px" tooltip="Administer prices/abailability" tooltip-placement="bottom" style="margin-right:20px; cursor: pointer; cursor: hand;">
					</td>
					<td class="div_changeprice" style="text-align:center">@{{item.name}}</td>
					<td class="div_changeprice" style="text-align:center">@{{item.special}}</td>
					<td class="div_changeprice" style="text-align:center"> </td>
					<td class="div_changeprice" style="text-align:center">@{{item.price_from}}</td>
					<td class="div_changeprice" style="text-align:center">@{{item.on_request}}</td>
					<td class="div_changeprice" style="text-align:center">
						<img ng-show="item.showNederland" ng-src="..\img\flags\nl111.jpg" width="20px" tooltip="Nederland" tooltip-placement="bottom" >
						<img ng-show="item.showDeutsch" ng-src="..\img\flags\du111.jpg" width="20px" tooltip="Deutsch" tooltip-placement="bottom" >
						<img ng-show="item.showEnglish" ng-src="..\img\flags\en111.jpg" width="20px" tooltip="English" tooltip-placement="bottom" >
						<img ng-show="item.showFrancais" ng-src="..\img\flags\fr111.jpg" width="20px" tooltip="Francais" tooltip-placement="bottom" >
					</td>
					<td>
						<button title="delete" type="button" class="btn btn-default fa fa-trash" ng-click="removeRow(item.name)" style="width:100%"></button>
					</td>
				</tr>
			</tbody>

		</table><br><br><br>
	</div>
</form>