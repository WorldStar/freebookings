<!-- Search box -->
{{--<div class="form-search">--}}
    {{--<form id="site-search" action="dashboard-v1">--}}
        {{--<input type="search" class="form-control" placeholder="Enter your query...">--}}
        {{--<button type="submit" class="fa fa-search"></button>--}}
    {{--</form>--}}
{{--</div>--}}


<!-- Site nav (vertical) -->

<nav class="site-nav clearfix" role="navigation" collapse-nav-accordion highlight-active>
    <ul class="nav-list">
        <li>
            <a href="#/home">
			<i class="fa fa-home icon" style="font-size:20px; color:#EBEBEB"></i>
            <span class="text" style="font-size:17px;">Dashboard</span></a>
        </li>
    </ul>
	@if( Auth::user()->type == "admin" )
    <ul class="nav-list">
        <li>
			<a href="javascript:;">
				<i class="fa fa-university" style="font-size:17px; color:#EBEBEB"></i>
				<span class="text" style="font-size:15px;">&nbsp;Hotels</span>
				<i class="arrow fa fa-angle-right right"></i>
			</a>
            <ul class="inner-drop">
				<li><a href="#/hotels" style="font-size:15px;">All Hotels</a></li>
				<li><a href="#/newhotel" style="font-size:15px;">Register Hotel</a></li>
				<li><a href="#/ui/icons" style="font-size:15px;">Invoices</a></li>
			</ul>
        </li>
    </ul>
    @endif	
	
    <div class="nav-title panel-heading" style=" color:#EBEBEB"><i>Your Hotel</i></div>
	
    <ul class="nav-list" data-ng-controller="roomsController">
		<li>
            <a href="javascript:;">
                <i class="fa fa-user icon" style="font-size:18px; color:#EBEBEB"></i>
                <span class="text" style="font-size:15px;color:#EBEBEB">Administrator</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop" ng-init="">
                <li>
					<a href="#" style="font-size:14px; color:#EBEBEB">Information</a>
				</li>
                <li>
					<a href="#" style="font-size:14px; color:#EBEBEB">Explanation</a>
				</li>
                <li>
					<a href="#" style="font-size:14px; color:#EBEBEB">FAQ</a>
				</li>
                <li>
					<a href="#" style="font-size:14px; color:#EBEBEB">Engine</a>
				</li>
                <li>
					<a href="#" style="font-size:14px; color:#EBEBEB">Own website</a>
				</li>
                
            </ul>
        </li>
       @if( Auth::user()->type != "admin" )
        <li>
			<a href="#/hotelData">
				<i class="fa fa-university" style="font-size:17px; color:#EBEBEB"></i>
				<span class="text" style="font-size:15px;">&nbsp;{{ Auth::user()->hotel_name }}</span>
			</a>
		</li>
		@endif
        <li>
            <a href="javascript:;">
                <i class="fa fa-list icon" style="font-size:18px; color:#EBEBEB"></i>
                <span class="text" style="font-size:15px;color:#EBEBEB">Room types</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop" ng-init="loadRooms()">
                <li><a href="#/addnewroom" style="font-size:14px; color:#EBEBEB">New room type<span class="badge badge-xs badge-primary right">new</span></a></li>
                <li ng-repeat="room in regRooms" ng-model="regRooms" ><a href="#/getRoom/@{{ room.name }}/@{{ room.id }}" style="font-size:14px; color:#EBEBEB">@{{ room.name }}</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav-list" data-ng-controller="arrangementsController">
        <li>
            <a href="javascript:;">
                <i class="fa fa-calendar icon" style="font-size:18px;color:#EBEBEB"></i>
                <span style="font-size:15px;color:#EBEBEB">Arrangements</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#/arrangements" style="font-size:14px;color:#EBEBEB">New arrangement</a></li>
                <li><a href="#/arrangements" style="font-size:14px;color:#EBEBEB">Arrangements</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav-list" >
        <li>
            <a href="javascript:;">
                <i class="fa fa-user icon" style="font-size:18px;color:#EBEBEB"></i>
                <span style="font-size:14px;color:#EBEBEB">Guests & Reservations</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#/guestsreservations" style="font-size:14px;color:#EBEBEB">Guests & Reservations</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Quick reservation</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Business guests</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Logies present</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Make offers</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav-list">
        <li>
            <a href="javascript:;">
                <i class="fa fa-info-circle icon" style="font-size:18px;color:#EBEBEB"></i>
                <span style="font-size:15px;color:#EBEBEB">information</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Rent</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Cost overview</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">invoice overview</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav-list">
        <li>
            <a href="javascript:;">
                <i class="fa fa-envelope-o icon" style="font-size:18px;color:#EBEBEB"></i>
                <span style="font-size:15px;color:#EBEBEB">Reports</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#" style="font-size:14px;color:#EBEBEB">Administer</a></li>
                <li><a href="#" style="font-size:14px;color:#EBEBEB">E-mail addresses</a></li>
            </ul>
        </li>
    </ul>
	
    <ul class="nav-list">
        <li>
            <a href="#">
                <i class="fa fa-hand-o-right icon" style="font-size:18px;color:#EBEBEB"></i>
                <span style="font-size:15px;color:#EBEBEB">Activities</span>
            </a>
        </li>
    </ul>
    <!--<ul class="nav-list">
        <li>
            <a href="javascript:;">
                <i class="fa fa-desktop icon"></i>
                <span>Blacklist</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#/addblacklist">add to blacklist</a></li>
                <li><a href="#/blacklist">view black list</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav-list">
        <li>
            <a href="javascript:;">
                <i class="fa fa-desktop icon"></i>
                <span>Informatioon about the city</span>
                <i class="arrow fa fa-angle-right right"></i>
            </a>
            <ul class="inner-drop">
                <li><a href="#">add to blacklist</a></li>
                <li><a href="#">Acitivities</a></li>
            </ul>
        </li>
    </ul>-->
    <ul class="nav-list">
        <li>
			
            <a href="../en/auth/logout">
			<i class="fa fa-sign-out icon" style="font-size:18px;color:#EBEBEB"></i>
			<span style="font-size:15px;color:#EBEBEB">Log out</span>
			</a>
        </li>
    </ul>
    

</nav>