
<script src="{{ URL::asset('/') }}js/plugins/vendors.js"></script>

<script src="{{ URL::asset('/') }}js/plugins/plugins.js"></script>
<script src="{{ URL::asset('/') }}js/flow/ng-flow-standalone.min.js"></script>
<script src="{{ URL::asset('/') }}js/spinner/angular-spinners.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/app.js"></script>

<script src="{{ URL::asset('/') }}js/plugins/moment.min.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/interact.min.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/angular-bootstrap-calendar-tpls.js"></script>

<script src="{{ URL::asset('/') }}js/plugins/angular-cookies.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/angular-translate.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/angular-translate-storage-cookie.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/angular-translate-storage-local.js"></script>
<script src="{{ URL::asset('/') }}js/plugins/angular-translate-loader-static-files.js"></script>
	
<script src="{{ URL::asset('/') }}js/angularJs/controllers/appControllers.js"></script>
<script src="{{ URL::asset('/') }}js/angularJs/controllers/hotelControllers.js"></script>
<script src="{{ URL::asset('/') }}js/angularJs/controllers/roomControllers.js"></script>
<script src="{{ URL::asset('/') }}js/angularJs/controllers/formControllers.js"></script>
<script src="{{ URL::asset('/') }}js/angularJs/controllers/arrangeControllers.js"></script>
<script src="{{ URL::asset('/') }}js/angularJs/controllers/guestsController.js"></script>


<script src="{{ URL::asset('/') }}js/angularJs/directives/appDirectives.js"></script>

<script src="{{ URL::asset('/') }}js/services/server.js"></script>