<form  name="newArrangeForm" class="form-horizontal col-lg-10 col-lg-offset-1" ng-init="_token='{{csrf_token()}}';"  ng-submit="arrangemtResult()" ng-show="!showRoom">

    <div class="form-group">
        <label class="col-lg-2 control-label">Name</label>
        <div class="col-lg-4" ng-model="_token">
            <input type="text" name="name" ng-model="Name" class="form-control" placeholder="Your Name" string-validity required>
            <span class="text-danger" ng-show="newArrangeForm.name.required">Enter Name Please</span>
            <span class="text-danger" ng-show="newArrangeForm.name.$error.string">Enter strings only</span>
        </div>		
		
    </div>
    <div class="form-group">
        <label class="col-lg-2 control-label" >	Trancription
		<span>	
			<img ng-src="..\img\flags\en111.jpg" width="25px" tooltip="English" tooltip-placement="bottom" >
		</span>
		</label>
        <div class="col-lg-10">
            <div  ng-model="transcription" class="text-angular-editor" text-angular></div>
        </div>
    </div>

	<div class="form-group">
        <label class="col-lg-2 control-label">Special</label>
        <div class="col-lg-2" style="width:110px">
			
		    <select name="mySelect" id="mySelect" class="form-control"
			    ng-options="option.name for option in specialData.availableOptions track by option.id"
			    ng-model="specialData.selectedOption">
			</select>
			
        </div>
	</div>

	<div class="form-group">
        <label class="col-lg-2 control-label">Maximum number</label>
        <div class="col-lg-2" style="width:110px">
		
			<select ng-model="adjust_maxDays"  class="form-control"
				ng-options="value for value in adjust_numbers">		
			</select>

        </div>
	</div>
	
	<div class="form-group">
        <label class="col-lg-2 control-label">Arrangements for serveral days</label>
        <div class="col-lg-10">
			<div style="float:left; width:80px">
			
				<select name="serveralDays" id="serveralDays" class="form-control"
					ng-options="option.name for option in serveralDaysData.availableOptions track by option.id"
					ng-model="serveralDaysData.selectedOption">
				</select>
			
			</div>
			<label class="col-lg-1 control-label" style="width:25%">this arrangement consists of </label>
			<div class="col-lg-10" style="width:110px">

				<select ng-model="adjust_numNights"  class="form-control"
					ng-options="value for value in adjust_numbers">		
				</select>

			</div>
			<label class="col-lg-offset-0" style="margin-top:5px">nights</label>
        </div>
	</div>

	<div class="form-group">
        <label class="col-lg-2 control-label pt0">Rebate System</label>
        <div class="col-lg-8">
            <div class="ui-radio ui-radio-primary mb0">
				
				<label class="ui-radio-inline" ng-repeat="choice in questions">
                    <input type="radio" name="rebate" ng-model="$parent.choiceRebate" ng-value="choice" ng-click="setQuestion(choice)"/>
                    <span>@{{choice.label}}&nbsp; &nbsp; </span>
                </label>
				
            </div>
        </div>
    </div>
	
	<div class="form-group">
        <label class="col-lg-2 control-label">Type</label>
        <div class="col-lg-2">
			<select name="multipleSelect" size="10" id="multipleSelect" ng-model="data.multipleSelect" multiple ng-change="onSelectService()">
				<option value="voorjaar"> spring </option>
				<option value="bosrijk"> bosrijk </option>
				<option value="zomer"> Summer </option>
				<option value="weekend"> weekend </option>
				<option value="zakelijk"> business </option>
				<option value="culinaire"> culinary </option>
				<option value="feestelijk"> festive</option>
				<option value="kindvriendelijk"> child-friendly </option>
				<option value="natuur_en_sprot"> nature and sports </option>
				<option value="romantisch"> romantic </option>
				<option value="verwen"> relax </option>
				<option value="Park_Sleep_Fly"> Park Sleep Fly </option>
				<option value="Aan_het_water"> Aan het water </option>	
			</select>
        </div><br><br><br>
		<p> You can select max. 3 types, press the CTRL-key and select the desired type. </p>
		<p><i> If you don't find your type arrangment, please inform us <span style="color:#ff9500">freebooking.nl.</span> </i></p>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label"></label>
		<div class="col-lg-2">
			<p>arrangement</p>
		</div>
    </div>
	
</form>




	
	

	

