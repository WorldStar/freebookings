	<script id="customMonthCell2.html" type="text/ng-template">									
		<div class="cal-month-day"  style="@{{day.style}}">
			<span
				class="pull-right"
				data-cal-date
				ng-click="vm.calendarCtrl.dateClicked(day.date)"											
				ng-bind="day.label">
			</span>

			<small style="position: absolute; bottom: 10px; left: 10px" ng-hide="day.show">
				<span style="float:left;font-size:13px;">€</span>
				<span style="margin-left:10px">
					<input type="text" ng-model="day.editcount" style="width:80%">
				</span>
			</small>

		</div>	
	</script>
	<b>
		Administration of the Price de kamer <i>twin kamer.</i>
		<br> <i>The last-Minute prices are not indicated here !</i> <br>
	</b>

	<h2 class="text-center"><span ng-bind="vm.calendarTitle"></span></h2> 										

	@include('admin.pages.calendarControls')
	<div class="div_changeprice">
		<div style="display: -webkit-flex;display: flex;">
			<div style="-webkit-flex: 1;flex: 1; -webkit-flex-direction: column; flex-direction: column;">
				<section style="height:29px;background-color:#fafafa">
					<p style="margin-left:10px; padding-top:5px"><b>Week</b></p>
				</section>
				<section style="height:101px; background-color:#fafafa;border-color:#ffffff" ng-repeat="week in vm.weeks track by $index">
					<p style="margin-left:10px; padding-top:10px"><b>@{{vm.weeknumber + $index}}</b></p>
				</section>																								
				
			</div>	
			<div style="-webkit-flex: 7;flex: 7;">
				<mwl-calendar
					events="vm.events"
					view="vm.calendarView"
					view-date="vm.viewDate"
					cell-is-open="vm.isCellOpen"
					view-title="vm.calendarTitle"		
					on-timespan-click="vm.timespanClicked(calendarDate)"
					cell-modifier="vm.cellModifier(calendarCell)"
					on-view-change-click="vm.viewChangeClicked(calendarNextView)"
					slide-box-disabled="true"
					>
				</mwl-calendar>
			</div>	
		</div>	
	</div><br>

	<table border = "0">
		<body>
			<tr>
				<td>
					please click on a day in order to select it or selelt a whole week.
					Change the price of the selected days in: €
					<span style="margin-left:3px"><input type="text" ng-model="vm.adjust_price"></span>
					<button  class="btn btn-primary" type="submit" ng-click="changeprice()">Change price</button>
				</td>
			</tr>
		</body>
	</table><br>
	<div class=" text-center">
		<div class="btn-group" >
			<button  class="btn btn-primary" type="submit" ng-click="vm.change()">Change</button>
			<button  class="btn btn-primary" type="submit" style="margin-left:20px" ng-click="vm.cancel()">Cancel</button>
		</div>
	</div><br>