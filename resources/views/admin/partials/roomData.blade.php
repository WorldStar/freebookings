<div class="page page-form-wizard clearfix ng-scope">

    <ol class="breadcrumb breadcrumb-small">
        <li>Your Hotel</li>
        <li class="active"><a href="#/hotels"> {{ $name }}</a></li>
    </ol>

    <div class="page-wrap">

        <div class="row">
            <div class="col-lg-12">
			
                <tabset>
                    <tab heading="Room Data" active="data.roomdata">
                        <div class="clearfix">
                            <div ng-controller="roomsEditController" ng-init="loadData( {{$id}} )" >
                                <spinner name="roomData" on-loaded="getloadspinner();">
                                    <div class="spinnerEdit">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                </spinner>
                                @include('admin.partials.room.roomForm')
                            </div>
                        </div>
                    </tab>
                    <tab heading="photos" active="data.photos">
                        <div class="clearfix">
                            <div ng-controller="roomsEditController" ng-init="loadRoomPhotos()">
                                @include('admin.partials.room.roomPhotos')
                            </div>
                        </div>
                    </tab>
                    <tab heading="Options" active="data.options">
                        <div class="clearfix">
                            <div ng-controller="roomsEditController" ng-init="loadRoomOptions()" >
                                <spinner name="roomOptions" on-loaded="loadOptionspinner();">
                                    <div class="spinnerEdit">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                </spinner>
                                @include('admin.partials.room.roomOptions')
                            </div>
                        </div>
                    </tab>
                    <tab heading="Price / Availability Management" active="data.availability" ng-controller="PriceTabCtrl as vm">
                        <div class="clearfix">
							
                            <tabset class="tabs-side">
                                <tab heading="Room Prices" active="data.tabsub1">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.availability.changeRoomPrice')
                                    </div>
                                </tab>
                                <tab heading="Room Availability" active="data.tabsub2">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.availability.changeRoomAvailability')
                                    </div>
                                </tab>
                                <tab heading="Room Open" active="data.tabsub3">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.availability.changeRoomOpen')
                                    </div>
                                </tab>
                                <tab heading="Room Close" active="data.tabsub4">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.availability.changeRoomClose')
                                    </div>
                                </tab>
                                <tab heading="Increase Available" active="data.tabsub5">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.availability.changeRoomIncreaseAvailability')
                                    </div>
                                </tab>


								<tab heading="Administer Availability" active="data.tabsub6" select="tabSelected1()">
									<div ng-controller="CustomTemplatesCtrl as vm">
                                       @include('admin.partials.room.availability.changeRoomAdminAvailability')
									</div>
                                </tab>
								
								<tab heading="Administer Price"  active="data.tabsub7" select="tabSelected2()">
									<div ng-controller="CustomTemplatesCtrl2 as vm">
                                       @include('admin.partials.room.availability.changeRoomAdminPrice')
                                    </div>
								</tab>
								
								<tab heading="Administer Price/Availability" active="data.tabsub8" select="tabSelected3()">
									<div ng-controller="CustomTemplatesCtrl3 as vm">
                                       @include('admin.partials.room.availability.changeRoomAdminPriceAvailability')
									</div>
                                </tab>
								
                            </tabset>

                        </div>
                    </tab>
                    <tab heading="Price Module"  active="data.price">
                        <div class="clearfix">
                            
                            <tabset class="tabs-side">
                                <tab heading="Last Minute">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.price-module.setLastMinutePrice')
                                    </div>
                                </tab>
                                <tab heading="Manage Discount Dates">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.price-module.setDiscountDates')
                                    </div>
                                </tab>
                                <tab heading="Exclusive Break-Fast">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.price-module.setExclusiveBreakfast')
                                    </div>
                                </tab>
                                <tab heading="Single Use">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.price-module.setSingleUse')
                                    </div>
                                </tab>
                                <tab heading="Non-Refundable">
                                    <div ng-controller="roomsEditController" class="clearfix" >
                                        @include('admin.partials.room.price-module.setNonRefundable')
                                    </div>
                                </tab>
                            </tabset>

                        </div>
                    </tab>

                    <tab heading="Minimum Stay"  active="data.stay">
                        <div class="clearfix">
                            <div ng-controller="roomsEditController"  >
                                @include('admin.partials.room.minStay')
                            </div>
                        </div>
                    </tab>


                </tabset>

                </div>
            </div>


    </div>

</div>
