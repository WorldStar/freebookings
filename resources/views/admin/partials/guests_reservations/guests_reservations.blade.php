
<div class="page page-form-wizard clearfix ng-scope">

    <div class="page-wrap">

        <div class="row">
            <div class="col-lg-12">

                <tabset>
                    <tab heading="Display all the guests" active="data.guests">
                        <div class="clearfix">

                            <div ng-controller="GuestsController" ng-init="loadGuests()">
                                @include( 'admin.partials.guests_reservations.guest-reservation.displayAllGuests' )
                            </div>

                        </div>
                    </tab>
                    <tab heading="Display all the reservations" active="data.reservations">
                        <div class="clearfix">
                            @include( 'admin.partials.guests_reservations.guest-reservation.displayAllReservation' )
                        </div>
                    </tab>
                    <tab heading="Administer a guest" active="data.adminGuest">
                        <div class="clearfix">
                            @include( 'admin.partials.guests_reservations.guest-reservation.administerGuest' )
                        </div>
                    </tab>
                    <tab heading="Administer a reservations" active="data.adminReservation">
                        <div class="clearfix">
                            @include( 'admin.partials.guests_reservations.guest-reservation.administerReservation' )
                        </div>
                    </tab>


                </tabset>

            </div>
        </div>


    </div>
</div>
