/**
 * Created by blue on 7/21/2016.
 */


guests.controller('GuestsController',["$scope", "Request", "spinnerService",
                                    function($scope, Request, spinnerService){

    $scope.data = {
        guests: true,
        reservations: false,
        adminGuest: false,
        adminReservation: true,
    };

    $scope.checkboxModel = {
        Nederland: false,
        English: false,
        Deutsch: false,
        Francais: false,
        removedItem: false,
        businessGuest: false,
    };

    // init dropdown control
    $scope.adjust_numbersFrom = [];
    for(var i = 0; i < 10; i++ )	$scope.adjust_numbersFrom[i] = i + 1;
    $scope.numberFrom = 0;

    $scope.adjust_numbersTo = [];
    for(var i = 0; i < 20; i++ )	$scope.adjust_numbersTo[i] = i + 1;
    $scope.numberTo = 0;

    $scope.showReservationNum = false;
    $scope.showInfo = false;
    $scope.showTable = true;
    $scope.showEmail = false;
    $scope.showBlackList = false;


    $scope.reservation = function(item) {
        $scope.showReservationNum = true;
        $scope.showInfo = false;
        $scope.showEmail = false;
        $scope.showBlackList = false;
    };

    $scope.information = function(item) {
        $scope.showReservationNum = false;
        $scope.showInfo = true;
        $scope.showEmail = false;
        $scope.showBlackList = false;
    };

    $scope.quickReservation = function(item) {

    };

    $scope.emailen = function(item) {
        $scope.showReservationNum = false;
        $scope.showInfo = false;
        $scope.showEmail = true;
        $scope.showBlackList = false;

    };

    $scope.blackList = function(item) {
        $scope.showReservationNum = false;
        $scope.showInfo = false;
        $scope.showEmail = false;
        $scope.showBlackList = true;

    };


    var testVal = $scope.checkboxModel.Nederland;
    var testVal1 = $scope.checkboxModel.removedItem;

    $scope.guestsData = [];

    $scope.gotoNewPage = function(name){

    }


    $scope.getAllGuests = function(){

        var newGuestData = {
            id: 2,
            hotel_id:   "2",
            gender:     "female",
            name:       "wang",
            address:    "Lionyoung Dandong",
            state:      "Lionyoung",
            city:       "Dandong",
            zip:        "118000",
            country:    "China",
            phone:      "18841568752",
            fax:        "",
            email:      "wangzhipeng@yahoo.com",
            language:   "En",
            benefit:    "No",
        }

        $scope.guestsData.push(newGuestData);

    }

    $scope.loadGuests = function ()
    {
        /*$scope.regRooms = regRooms;
         while($scope.regRooms.length > 0) {
         $scope.regRooms.pop();
         }*/
        //spinnerService.show('html5spinner');

        //var res = Request.post("get",   "loadRooms",  "");
        //res.then(
        //    function successCallback(response){
        //        $scope.regRooms = regRooms;
        //
        //        for (var key in response.data.rooms) {
        //            var arr = response.data.rooms[key];
        //            $scope.regRooms.push(arr);
        //        }
        //        spinnerService.hide('html5spinner');
        //    },
        //    function errorCallback(response) {
        //        //$scope.loadRooms();
        //    }
        //);
    }

}]);
