	<script id="customMonthCell.html" type="text/ng-template">									
		<div class="cal-month-day"  style="@{{day.style}}">
			<span
				class="pull-right"
				data-cal-date
				ng-click="vm.calendarCtrl.dateClicked(day.date)"											
				ng-bind="day.label">
			</span>

			<small style="position: absolute; bottom: 25px; left: 10px" >
			   <p> <input type="text" ng-model="day.editcount" ng-disabled="day.checked" style="width:90%"></p>
			</small>

			<small style="position: absolute; bottom: 10px; left: 10px">
				<span ng-bind="day.count"></span> 
			</small>
		</div>	
	</script>

	<b>
		Administrater the availability of de kamer <i>twin kamer.</i>
		<br><br>
	</b>
	
	<h2 class="text-center">@{{vm.calendarTitle}}</h2> 										
	
	@include('admin.pages.calendarControls')
	<div class="div_changeprice">
		<div style="display: -webkit-flex;display: flex;">
			<div style="-webkit-flex: 1;flex: 1; -webkit-flex-direction: column; flex-direction: column;">
				<section style="height:29px;background-color:#fafafa">
					<p style="margin-left:10px; padding-top:5px"><b>Week</b></p>
				</section>
				<section style="height:101px; background-color:#fafafa;border-color:#ffffff" ng-repeat="week in vm.weeks track by $index">
					<p style="margin-left:10px; padding-top:10px"><b>@{{vm.weeknumber + $index}}</b></p>
						<p style="margin-left:10px">
							<span>Available</span>
							<select ng-model="vm.adjust_value_array[$index]"
								ng-options="value for value in vm.adjust_numbers">		
							</select>
						</p >
					<p style="margin-left:10px;margin-top:-5px"><b>Rent</b></p>
				</section>																								
				
			</div>	
			<div style="-webkit-flex: 7;flex: 7;">
				<mwl-calendar
					events="vm.events"
					view="vm.calendarView"
					view-date="vm.viewDate"
					cell-is-open="vm.isCellOpen"
					view-title="vm.calendarTitle"
					on-timespan-click="vm.timespanClicked(calendarDate)"
					cell-modifier="vm.cellModifier(calendarCell)"
					on-view-change-click="vm.viewChangeClicked(calendarNextView)"
					slide-box-disabled="true"
					>
				</mwl-calendar>
			</div>	
		</div>
	</div><br>

	<table border = "0">
		<body>
			<tr>
				<td>
					please check on the day you wish to choose, or click on the week to choose all the days.
					Change the abailiabliaty of the selected days to:
					<select ng-model="vm.adjust_value"
						ng-options="value for value in vm.adjust_numbers">		
					</select>
					
					<button  class="btn btn-primary" type="submit" ng-click="vm.adjust()">Adjust</button>
					
				</td>
			</tr>
		</body>
	</table>
	<br>
	<div class=" text-center">
		<div class="btn-group" >
			<button  class="btn btn-primary" type="submit"  ng-click="vm.change()">Change</button>
			<button  class="btn btn-primary" type="submit" style="margin-left:20px" ng-click="vm.cancel()">Cancel</button>
		</div>
	</div>
	<br>
