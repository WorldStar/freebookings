
/*** Arrangements Controllers
 ------------------------------------------------ ****/

// arrangements.controller("ArrangementController", ["$scope", function ($scope) {
// $scope.arrangements = 'test arrangements';
// }]);

/*** Config for translation ***/
uiFormCtrls.config(['$translateProvider', function($translateProvider){
	// Register a loader for the static files
	// So, the module will search missing translation tables under the specified urls.
	// Those urls are [prefix][langKey][suffix].
	$translateProvider.useStaticFilesLoader({
		prefix: '/l10n/',
		suffix: '.js'
	});
	// Tell the module what language to use by default
	$translateProvider.preferredLanguage('en');
	// Tell the module to store the language in the local storage
	$translateProvider.useLocalStorage();
}]);


/*** FormWizard Controller ***/
//uiFormCtrls.controller("FormWizardCtrl", ["$scope", "$http", "$translate", function($scope, $http, $translate) {
arrangements.controller("ArrangementController", ["$scope", "spinnerService", "Request",
													function($scope, spinnerService, Request) {

	// get data from DB
	var res = Request.post("get", "api/arrangements/2/hotel-arrangement", "");
	res.then(
			function successCallback(response) {
				console.log(response);
				var k = 0;
				for (var key in response.data) {
					var arr = response.data[key];
					var temppatroon = arr.patroon.split('-');
					var tempLanguage = arr.language.split(',');
					var tempType = arr.type.split(',');
					var tempPrice = arr.price.split('|');
					var tempRebate = parseInt(arr.rebate);
					var tempRequest = parseInt(arr.on_request);
					arr.patroon = temppatroon;
					arr.language = tempLanguage;
					arr.type = tempType;
					arr.price = tempPrice;
					arr.rebate = tempRebate;
					arr.on_request = tempRequest;

					for(var p=0; p< arr.language.length; p++) {
						if (arr.language[p] == 'Ne') arr.showNederland = true;
						if (arr.language[p] == 'En') arr.showEnglish = true;
						if (arr.language[p] == 'De') arr.showDeutsch = true;
						if (arr.language[p] == 'Fr') arr.showFrancais = true;
					}
					$scope.arrangementData.push(arr);
				}
				spinnerService.hide('html5spinner');
			},
			function errorCallback(response) {
				$scope.loadRooms();
			}
	);

	//	pagenation for tap
	$scope.steps = [!0, !1, !1], $scope.stepNext = function(index) {
		for (var i = 0; i < $scope.steps.length; i++) $scope.steps[i] = !1;
		$scope.steps[index] = !0;
	},
			$scope.stepReset = function() { $scope.steps = [!0, !1, !1] },

		// definition for special data
			$scope.specialData = {
				availableOptions: [
					{id: 0, name: 'Yes'},
					{id: 1, name: 'No'},
				],
				selectedOption: {id: 1, name: 'No'}, //This sets the default value of the select in the ui
			};

	// definition for serveral days data
	$scope.serveralDaysData = {
		availableOptions: [
			{id: '0', name: 'Yes'},
			{id: '1', name: 'No'},
		],
	};
	$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[1];

	// init dropdown control
	$scope.adjust_numbers = [];
	$scope.adjust_nights = [];

	for(var i = 0; i < 20; i++ )	$scope.adjust_numbers[i] = i + 1;

	$scope.adjust_maxDays = 1;
	$scope.adjust_numNights = 1;

	// choice of nights
	$scope.questions = [
		{id: 0, isanswer: true, label: 'None'},
		{id: 1, isanswer: false, label: '3=2, Book 3 nights = Pay for 2 nights(multiple 3 nights)'},
	];
	$scope.choiceRebate = $scope.questions[0];


	$scope.setQuestion = function(question) {
		angular.forEach($scope.questions, function(q) {
			q.isanswer = false; //set them all to false
		});
		question.isanswer = true; //set the clicked one to true

		var index = $scope.questions.indexOf(question);
		if( index == 1 )
		{
			$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[0];
			$scope.adjust_numNights = 3;
			$scope.choiceRebate = $scope.questions[1];
		}else{
			$scope.choiceRebate = $scope.questions[0];
		}
	};

	// choice for price
	$scope.priceQuestions = [
		{id: 0, isanswer: true, priceLabel: 'Ja, arrangment (meer)prijs per dag:', editPrice: '', endLabel:'' },
		{id: 1, isanswer: false, priceLabel: 'Nee, Prices from:', editPrice: '', endLabel:'(*1)'},
	];

	$scope.priceroom = $scope.priceQuestions[1];

	$scope.setPriceQuestion = function(question) {
		angular.forEach($scope.priceQuestions, function(q) {
			q.isanswer = false; //set them all to false
		});
		question.isanswer = true; //set the clicked one to true

		var index = $scope.priceQuestions.indexOf(question);
		if( index == 0 ){
			$scope.priceroom = $scope.priceQuestions[0];
			$scope.showHide1 = true;
			$scope.priceQuestions[1].editPrice = '';
		}else{
			$scope.priceroom = $scope.priceQuestions[1];
			$scope.showHide1 = false;
			$scope.priceQuestions[0].editPrice = '';
			$scope.price1 = '';
			$scope.price2 = null;
		}
	};

	// choice for standard room
	$scope.standardQuestions = [
		{id: 0, isanswer: true, label: 'Standard room'},
		{id: 1, isanswer: false, label: 'Unstandard room'},
	];

	$scope.setStandardQuestion = function(question) {
		angular.forEach($scope.standardQuestions, function(q) {
			q.isanswer = false; //set them all to false
		});
		question.isanswer = true; //set the clicked one to true

		var index = $scope.standardQuestions.indexOf(question);

		// if ($scope.flag001 == true )	$scope.flag001 = false;
		// if ($scope.flag002 == true )	$scope.flag002 = false;

		if( index == 0 ) {
			$scope.checkboxModel.twin = true;
			$scope.choiceOfStandardRoom = $scope.standardQuestions[0];
		}

		if( index == 1 ){
			$scope.checkboxModel.test = true;
			$scope.choiceOfStandardRoom = $scope.standardQuestions[1];
		}

	};

	// multiple select for type
	$scope.data = {
		singleSelect: null,
		multipleSelect: [],
		option1: 'option-1',
	};

	$scope.forceUnknownOption = function() {
		$scope.data.singleSelect = 'nonsense';
	};

	$scope.onSelectService = function() {
		if( $scope.data.multipleSelect.length > 3 )
			$scope.data.multipleSelect = [];
	}

	$scope.requestData = {
		availableOptions: [
			{id: 0, name: 'Yes'},
			{id: 1, name: 'No'},
		],
		selectedOption: {id: 1, name: 'No'}, //This sets the default value of the select in the ui
	};
	$scope.requestData.selectedOption = $scope.requestData.availableOptions[1];

	$scope.checkboxModel = {
		twin: false,
		test: false,
		Monday: false,
		Tuesday: false,
		Wednesday: false,
		Thursday: false,
		Friday: false,
		Saturday: false,
		Sunday: false,
		Nederland: false,
		English: false,
		Deutsch: false,
		Francais: false,
		availability: false
	};

	$scope.showAdd = true;
	$scope.showSave = false;

	// convert for date format
	function convert(str) {
		var date = new Date(str),
				mnth = ("0" + (date.getMonth()+1)).slice(-2),
				day  = ("0" + date.getDate()).slice(-2);
		return [ date.getFullYear(), mnth, day ].join("-");
	}


	function init() {
		$scope.Name = '';
		$scope.transcription = '';
		$scope.specialData.selectedOption = $scope.specialData.availableOptions[1];
		$scope.adjust_maxDays = 1;
		$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[1];
		$scope.adjust_numNights = 1;
		$scope.choiceRebate = $scope.questions[0];
		$scope.data.multipleSelect = [];
		$scope.priceroom = $scope.priceQuestions[1];
		$scope.priceQuestions[0].editPrice = null;
		$scope.priceQuestions[1].editPrice = null;
		$scope.choiceOfStandardRoom = null;
		$scope.price1 = null;
		$scope.price2 = null;
		$scope.checkboxModel.twin = false;
		$scope.checkboxModel.test = false;
		$scope.checkboxModel.Monday = false;
		$scope.checkboxModel.Tuesday = false;
		$scope.checkboxModel.Wednesday = false;
		$scope.checkboxModel.Thursday = false;
		$scope.checkboxModel.Friday = false;
		$scope.checkboxModel.Saturday = false;
		$scope.checkboxModel.Sunday = false;
		$scope.checkboxModel.Nederland = false;
		$scope.checkboxModel.English = false;
		$scope.checkboxModel.Deutsch = false;
		$scope.checkboxModel.Francais = false;
		$scope.checkboxModel.availability = false;
		$scope.requestData.selectedOption = $scope.requestData.availableOptions[1];

		$scope.dateFrom = null;
		$scope.dateUntil = null;
		//$scope.reserved = '';
		//$scope.status = '';
		$scope.showNederland = false;
		$scope.showEnglish = false;
		$scope.showDeutsch = false;
		$scope.showFrancais = false;

	}


	// tables in local
	$scope.arrangementData = [];

	var indexSave = -1;

	// add data to DB
	$scope.Add = function() {
		if($scope.Name) {
			if ($scope.dateFrom != undefined && $scope.dateUntil != undefined ){
				if ($scope.choiceOfStandardRoom) {
					var priceArray = [];
					var dayofweek = [];
					var languagelink = [];
					var roomArray = [];

					priceArray[0] = parseInt($scope.priceQuestions[0].editPrice);
					priceArray[1] = parseInt($scope.price1);
					priceArray[2] = parseInt($scope.price2);

					roomArray[0] = $scope.checkboxModel.twin;
					roomArray[1] = $scope.checkboxModel.test;

					var k = 0;
					if ($scope.checkboxModel.Monday) 	{ dayofweek[k] = 'Mon';	k++; }
					if ($scope.checkboxModel.Tuesday) 	{ dayofweek[k] = 'Tue'; k++; }
					if ($scope.checkboxModel.Wednesday) { dayofweek[k] = 'Wed'; k++; }
					if ($scope.checkboxModel.Thursday) 	{ dayofweek[k] = 'Thu';	k++; }
					if ($scope.checkboxModel.Friday) 	{ dayofweek[k] = 'Fri';	k++; }
					if ($scope.checkboxModel.Saturday) 	{ dayofweek[k] = 'Sat';	k++; }
					if ($scope.checkboxModel.Sunday) 	{ dayofweek[k] = 'Sun';	k++; }
					var m = 0;
					if ($scope.checkboxModel.Nederland) { languagelink[m] = 'Ne'; m++; }
					if ($scope.checkboxModel.English) 	{ languagelink[m] = 'En'; m++; }
					if ($scope.checkboxModel.Deutsch)	{ languagelink[m] = 'De'; m++; }
					if ($scope.checkboxModel.Francais)	{ languagelink[m] = 'Fr'; m++; }

					//var trans = [];
					//var trans1 = [];
					//var tranStr = '';
					//if($scope.transcription) {
					//	trans = $scope.transcription.split('</p>');
					//	trans1 = trans[0].split('<p>');
					//	if(trans1[0] == '' && trans1[1] != '') tranStr = trans1[1];
					//	else tranStr =  trans1[0];
					//}else{
					//	tranStr = '';
					//}
					if($scope.transcription == undefined)  $scope.transcription = '';

					var testVal = '';
					if($scope.checkboxModel.availability) testVal = '1';
					else testVal = '0';

					if (dayofweek.length > 0) {
						if (languagelink.length > 0) {
							var newArrangementData = {
								id: 2,
								hotel_id: "2",
								name: $scope.Name,
								rooms: roomArray,
								special: $scope.specialData.selectedOption.id,
								persons: "",
								price: priceArray,
								date_from: convert($scope.dateFrom),
								date_to: convert($scope.dateUntil),
								patroon: dayofweek,
								standard_room: $scope.choiceOfStandardRoom.id,
								price_from: $scope.priceQuestions[1].editPrice,
								maximum_available: $scope.adjust_maxDays,
								on_request: $scope.requestData.selectedOption.id,
								language: languagelink,
								more_days: $scope.serveralDaysData.selectedOption.id,
								nights: $scope.adjust_numNights,
								type: $scope.data.multipleSelect,
//								discount_type: "",
								discount_type: $scope.priceroom.id.toString(),
								linked_rooms_available: testVal,
								extra_price_with_room_price: "",
								_token: $scope._token,

								// Now there is not in DB. So insert item in the DB
								description: $scope.transcription,
								rebate : $scope.choiceRebate.id,
							}

							// request to server
							spinnerService.show('html5spinner');

							var res = Request.post("post", "api/arrangements/2/hotel-arrangement", newArrangementData);

							res.then(
								function successCallback(response) {
									// add to local table
									newArrangementData.id = response.data.Arrangement.id;
									$scope.arrangementData.push(newArrangementData);

									var newItem = $scope.arrangementData[$scope.arrangementData.length - 1];
									if ($scope.checkboxModel.Nederland)    newItem.showNederland = true;
									if ($scope.checkboxModel.English)      newItem.showEnglish = true;
									if ($scope.checkboxModel.Deutsch)      newItem.showDeutsch = true;
									if ($scope.checkboxModel.Francais)     newItem.showFrancais = true;
									//index = response.data.Arrangement.id;
									init();

									//$scope.message = response.data.flash.message;
									swal({
										title: response.data.flash.title,
										text: response.data.flash.message,
										type: response.data.flash.type,
										timer: 2000,
										showConfirmButton: false
									});
									spinnerService.hide('html5spinner');

								},
								function errorCallback(response) {
									spinnerService.hide('html5spinner');
								}
							);
						}else alert('Sorry, please check at least one for link to in validity page.');
					}else alert('Sorry, please check at least one for valid on in validity page.');
				}else alert('Sorry, please check standard room in Price and Availability page.');
			}else alert('Sorry, please input dates in validity page.');
		}else alert('Sorry, please input your name in basic info page.');
	}


	// delete data in DB
	$scope.removeRow = function(name){
		var index = -1;
		var comArr = eval( $scope.arrangementData );
		for( var i = 0; i < comArr.length; i++ ) {
			if( comArr[i].name === name ) {
				index = i;
				break;
			}
		}
		if( index === -1 ) {
			alert( "Something gone wrong" );
		}

		// delete request to server
		var deleteId = $scope.arrangementData[index].id;
		var newArrangementData = {
			hotel_id:2,
			arrangement_id: deleteId,
			_method:"delete"
		}

		spinnerService.show('html5spinner');

		var res = Request.post("post", "api/arrangements/2/hotel-arrangement/" + deleteId, newArrangementData);

		res.then(
				function successCallback(response){

					//$scope.message = response.data.flash.message;

					swal({
						title:response.data.flash.title,
						text: response.data.flash.message,
						type: response.data.flash.type,
						timer: 2000,
						showConfirmButton:false
					});
					spinnerService.hide('html5spinner');
				},
				function errorCallback(response) {
					spinnerService.hide('html5spinner');
				}
		);

		$scope.arrangementData.splice( index, 1 );
		init();
	};



	$scope.changeArrangement = function(name) {
		/* the toggling logic */

		$scope.showSave = true;
		$scope.showAdd = false;

		var test = $scope.priceQuestions[0].editPrice;
		var test1 = $scope.priceQuestions[1].editPrice;

		var comArr = eval( $scope.arrangementData );
		for( var i = 0; i < comArr.length; i++ ) {
			if( comArr[i].name === name ) {
				index = i;
				break;
			}
		}
		if( index === -1 ) {
			alert( "Something gone wrong" );
		}
		indexSave = index;
		$scope.Name = comArr[index].name;
		$scope.transcription = comArr[index].description;
		if(comArr[index].special == 0 ){
			$scope.specialData.selectedOption = $scope.specialData.availableOptions[0];
		}else{
			$scope.specialData.selectedOption = $scope.specialData.availableOptions[1];
		}

		if(comArr[index].discount_type == '0'){
			$scope.priceroom = $scope.priceQuestions[0];
			$scope.showHide1 = true;

		}else{
			$scope.priceroom = $scope.priceQuestions[1];
			$scope.showHide1 = false;
		}

		$scope.priceQuestions[0].editPrice = comArr[index].price[0];
		$scope.price1 = comArr[index].price[1];
		$scope.price2 = comArr[index].price[2];
		$scope.dateFrom = comArr[index].date_from;
		$scope.dateUntil = comArr[index].date_to;

		for( var i = 0; i < comArr[index].patroon.length; i++ ) {
			if( comArr[index].patroon[i] == 'Mon' ) $scope.checkboxModel.Monday = true;
			if( comArr[index].patroon[i] == 'Tue' ) $scope.checkboxModel.Tuesday = true;
			if( comArr[index].patroon[i] == 'Wed' ) $scope.checkboxModel.Wednesday = true;
			if( comArr[index].patroon[i] == 'Thu' ) $scope.checkboxModel.Thursday = true;
			if( comArr[index].patroon[i] == 'Fri' ) $scope.checkboxModel.Friday = true;
			if( comArr[index].patroon[i] == 'Sat' ) $scope.checkboxModel.Saturday = true;
			if( comArr[index].patroon[i] == 'Sun' ) $scope.checkboxModel.Sunday = true;
		}
		$scope.checkboxModel.twin = comArr[index].rooms[0];
		$scope.checkboxModel.test = comArr[index].rooms[1];

		if(comArr[index].standard_room){
			$scope.checkboxModel.test = true;
			$scope.choiceOfStandardRoom = $scope.standardQuestions[1];
		}else{
			$scope.checkboxModel.twin = true;
			$scope.choiceOfStandardRoom = $scope.standardQuestions[0];
		}
		$scope.priceQuestions[1].editPrice =  comArr[index].price_from;
		$scope.adjust_maxDays = comArr[index].maximum_available;
		if(comArr[index].on_request == 0)
			$scope.requestData.selectedOption = $scope.requestData.availableOptions[0];
		else
			$scope.requestData.selectedOption = $scope.requestData.availableOptions[1];

		for( var j = 0; j < comArr[index].language.length; j++ ) {
			if (comArr[index].language[j] == 'Ne') {
				$scope.checkboxModel.Nederland = true;
				$scope.showNederland = true;
			}
			if (comArr[index].language[j] == 'En') {
				$scope.checkboxModel.English = true;
				$scope.showEnglish = true;
			}
			if (comArr[index].language[j] == 'De') {
				$scope.checkboxModel.Deutsch = true;
				$scope.showDeutsch = true;
			}
			if (comArr[index].language[j] == 'Fr') {
				$scope.checkboxModel.Francais = true;
				$scope.showFrancais = true;
			}
		}

		if(comArr[index].rebate == 1){
			$scope.choiceRebate = $scope.questions[1];
			//$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[0];
			//$scope.adjust_numNights = 3;
		}else{
			$scope.choiceRebate = $scope.questions[0];
		}

		if(comArr[index].more_days == '0'){
			$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[0];
		}
		if(comArr[index].more_days == '1'){
			$scope.serveralDaysData.selectedOption = $scope.serveralDaysData.availableOptions[1];
		}
		$scope.adjust_numNights = comArr[index].nights;
		$scope.data.multipleSelect = comArr[index].type;
		if(comArr[index].linked_rooms_available == '0' ) {
			$scope.checkboxModel.availability = false;
		}else{
			$scope.checkboxModel.availability = true;
		}
		$scope._token = comArr[index]._token;

	};

	//var newArrangementData = {
	//    id: 2,
	//    hotel_id : "2",
	//    name : $scope.Name,
	//    rooms: roomsDummy,
	//    special : 0,
	//    persons : "",
	//    price : priceDummy,
	//    date_from : "2016-05-12",
	//    date_to : "2016-05-31",
	//    patroon : patroonDummy,
	//    standard_room : "",
	//    price_from : "",
	//    maximum_available: "",
	//    on_request: 0,
	//    language: languageDummy,
	//    more_days: "",
	//    nights: "",
	//    type: typeDummy,
	//    discount_type: "",
	//    linked_rooms_available: "",
	//    extra_price_with_room_price: "",
	//    _token: $scope._token,
	//    description: "I'm stupid description"
	//}


	// Save
	$scope.Save = function() {

		var patroonDummy = [];
		var languageDummy = [];
		var typeDummy = [];
		var priceDummy = [];
		var roomsDummy = [];

		priceDummy[0] = parseInt($scope.priceQuestions[0].editPrice);
		priceDummy[1] = parseInt($scope.price1);
		priceDummy[2] = parseInt($scope.price2);

		roomsDummy[0] = $scope.checkboxModel.twin;
		roomsDummy[1] = $scope.checkboxModel.test;

		var k = 0;
		if( $scope.checkboxModel.Monday ) 		{ patroonDummy.push('Mon'); k++; }
		if( $scope.checkboxModel.Tuesday ) 		{ patroonDummy.push('Tue'); k++; }
		if( $scope.checkboxModel.Wednesday ) 	{ patroonDummy.push('Wed'); k++; }
		if( $scope.checkboxModel.Thursday ) 	{ patroonDummy.push('Thu'); k++; }
		if( $scope.checkboxModel.Friday ) 		{ patroonDummy.push('Fri'); k++; }
		if( $scope.checkboxModel.Saturday ) 	{ patroonDummy.push('Sat'); k++; }
		if( $scope.checkboxModel.Sunday ) 		{ patroonDummy.push('Sun'); k++; }
		var m = 0;
		if( $scope.checkboxModel.Nederland ) 	{ languageDummy.push('Ne'); m++; }
		if( $scope.checkboxModel.English ) 		{ languageDummy.push('En'); m++; }
		if( $scope.checkboxModel.Deutsch ) 		{ languageDummy.push('De'); m++; }
		if( $scope.checkboxModel.Francais ) 	{ languageDummy.push('Fr'); m++; }

		var putId = $scope.arrangementData[indexSave].id;

		for (var i=0; i < $scope.data.multipleSelect.length; i++) {
			if($scope.data.multipleSelect[i]) {
				var temp = $scope.data.multipleSelect[i];
				typeDummy.push(temp);
			}
		}

		//var trans = [];
		//var trans1 = [];
		//var tranStr = '';
		//if($scope.transcription) {
		//	trans = $scope.transcription.split('</p>');
		//	trans1 = trans[0].split('<p>');
		//	if(trans1[0] == '' && trans1[1] != '') tranStr = trans1[1];
		//	else tranStr =  trans1[0];
		//}else{
		//	tranStr = '';
		//}
		if($scope.transcription == undefined)  $scope.transcription = '';

		var testVal = '';
		if($scope.checkboxModel.availability) testVal = '1';
		else testVal = '0';


		var newArrangementData = {
			id: putId,
			hotel_id : "2",
			name : $scope.Name,
			rooms: roomsDummy,
			special : $scope.specialData.selectedOption.id,
			persons : "",
			price : priceDummy,
			date_from : convert($scope.dateFrom),
			date_to : convert($scope.dateUntil),
			patroon : patroonDummy,
			standard_room : $scope.choiceOfStandardRoom.id,
			price_from : $scope.priceQuestions[1].editPrice,
			maximum_available: $scope.adjust_maxDays,
			on_request: $scope.requestData.selectedOption.id,
			language: languageDummy,
			more_days: $scope.serveralDaysData.selectedOption.id,
			nights: $scope.adjust_numNights,
			type: typeDummy,
			discount_type: $scope.priceroom.id.toString(),
			linked_rooms_available: testVal,
			extra_price_with_room_price: "",
			_token: $scope._token,
			description: $scope.transcription,
			rebate : $scope.choiceRebate.id,
		}

		// request to server
		spinnerService.show('html5spinner');
		var res = Request.post("put", "api/arrangements/2/hotel-arrangement/" + putId, newArrangementData);

		res.then(
			function successCallback(response){

				var newItem = $scope.arrangementData[indexSave];
				newItem.showNederland = false;
				newItem.showEnglish = false;
				newItem.showDeutsch = false;
				newItem.showFrancais = false;
				if ($scope.checkboxModel.Nederland)    newItem.showNederland = true;
				if ($scope.checkboxModel.English)      newItem.showEnglish = true;
				if ($scope.checkboxModel.Deutsch)      newItem.showDeutsch = true;
				if ($scope.checkboxModel.Francais)     newItem.showFrancais = true;
				init();

				swal({
					title:response.data.flash.title,
					text: response.data.flash.message,
					type: response.data.flash.type,
					timer: 2000,
					showConfirmButton:false
				});
				spinnerService.hide('html5spinner');
			},
			function errorCallback(response) {
				spinnerService.hide('html5spinner');
			}
		);
		$scope.arrangementData[index].name = newArrangementData.name;
		$scope.arrangementData[index].description = newArrangementData.description;
		$scope.arrangementData[index].rooms = newArrangementData.rooms;
		$scope.arrangementData[index].special = newArrangementData.special;
		$scope.arrangementData[index].price = newArrangementData.price;
		$scope.arrangementData[index].date_from = newArrangementData.date_from;
		$scope.arrangementData[index].date_to = newArrangementData.date_to;
		$scope.arrangementData[index].patroon = newArrangementData.patroon;
		$scope.arrangementData[index].standard_room = newArrangementData.standard_room;
		$scope.arrangementData[index].price_from = newArrangementData.price_from;
		$scope.arrangementData[index].maximum_available = newArrangementData.maximum_available;
		$scope.arrangementData[index].on_request = newArrangementData.on_request;
		$scope.arrangementData[index].language = newArrangementData.language;
		$scope.arrangementData[index].more_days = newArrangementData.more_days;
		$scope.arrangementData[index].nights = newArrangementData.nights;
		$scope.arrangementData[index].type = newArrangementData.type;
		$scope.arrangementData[index].rebate = newArrangementData.rebate;
		$scope.arrangementData[index].discount_type = newArrangementData.discount_type;
		//
		// change for buttons tag
		$scope.showSave = false;
		$scope.showAdd = true;

	}

	$scope.Cancel1 = function() {
		init();
	}

	$scope.Cancel2 = function() {
		init();
		$scope.showSave = false;
		$scope.showAdd = true;
	}

	// create the module and name it scotchApp    
	var scotchApp = angular.module('RoutingApp', ['ngRoute']);


	// pagenation to rooms tap pages
	var fromdate = "";
	var untildate = "";
	var adjust_maxDays = "";
	var price = "";

	$scope.adminAvailibility = function(item) {
		fromdate = item.date_from;
		untildate = item.date_to;
		adjust_maxDays = item.maximum_available;
		document.cookie = "selectval=1," + fromdate + ","
				+ untildate + ","
				+ adjust_maxDays + ","
				+ price;
		window.location='#/getRoom/' + item.name + '/1';
//		window.location='#/getRoom/jhon/1';
	};

	$scope.adminPrices = function(item) {
		fromdate = item.date_from;
		untildate = item.date_to;
		adjust_maxDays = item.maximum_available;
		if(item.price[0]){
			price = item.price[0];
		}
		if(item.price_from){
			price = item.price_from;
		}
		document.cookie = "selectval=2," + fromdate + ","
				+ untildate + ","
				+ adjust_maxDays + ","
				+ price;
		window.location='#/getRoom/' + item.name + '/1';
//		window.location='#/getRoom/jhon/1';
	};

	$scope.adminPriceAvalability = function(item) {
		fromdate = item.date_from;
		untildate = item.date_to;
		adjust_maxDays = item.maximum_available;
		if(item.price[0]){
			price = item.price[0];
		}
		if(item.price_from){
			price = item.price_from;
		}
		document.cookie = "selectval=3," + fromdate + ","
				+ untildate + ","
				+ adjust_maxDays + ","
				+ price;
		window.location='#/getRoom/' + item.name + '/1';
// 		window.location='#/getRoom/jhon/1';
	};


}]);
