<form  name="newArrangeForm2" class="form-horizontal col-lg-10 col-lg-offset-1" ng-submit="arrangemtResult()" ng-show="!showRoom">
	<div class="form-group mb0" ng-controller="DatepickerCtrl">
		<label class="col-lg-2 control-label">Select Dates</label>
		<div class="col-lg-4">
			<div class="input-group">
				<input type="text" class="form-control" ng-model="$parent.dateFrom" name="from" datepicker-popup is-open="fromOpened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" show-button-bar="false" placeholder="From">
				<span class="input-group-btn">
					<button type="button" class="btn btn-sm btn-info fa fa-calendar" ng-click="fromOpen($event)"></button>
				</span>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="input-group mb0">
				<input type="text" class="form-control" ng-model="$parent.dateUntil" name="to" datepicker-popup is-open="toOpened" min-date="minDate" max-date="maxDate" datepicker-options="dateOptions" date-disabled="disabled(date, mode)" ng-required="true" show-button-bar="false" placeholder="Until">
				<span class="input-group-btn">
					<button type="button" class="btn btn-sm btn-info fa fa-calendar" ng-click="toOpen($event)"></button>
				</span>
			</div>
		</div>
	</div>
	
	<div class="form-group mb0">
		<label class="col-lg-2 control-label" style="margin-top:-10px">Valid on</label>
		<div class="col-lg-4">
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Monday">
					<span>Monday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Tuesday">
					<span>Tuesday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Wednesday">
					<span>Wednesday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Thursday">
					<span>Thursday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Friday">
					<span>Friday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Saturday">
					<span>Saturday</span>
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Sunday">
					<span>Sunday</span>
				</label>
			</div>
		</div>
	</div>
	<div class="form-group mb0">
		<label class="col-lg-2 control-label" style="margin-top:-10px">Link to</label>
		<div class="col-lg-4">
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox"  ng-model="checkboxModel.Nederland">
					<span><img ng-src="..\img\flags\nl111.jpg" width="20px" tooltip="Nederland" tooltip-placement="bottom" >&nbsp; <span translate="header.navbar.Dutdh">Dutdh</span></span> 
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Deutsch">
					<span><img ng-src="..\img\flags\du111.jpg" width="20px" tooltip="Deutsch" tooltip-placement="bottom" >&nbsp; <span translate="header.navbar.German">German</span></span> 
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.English" >
					<span><img ng-src="..\img\flags\en111.jpg" width="20px" tooltip="English" tooltip-placement="bottom" >&nbsp;<span translate="header.navbar.English">English</span></span> 
				</label>
			</div>
			<div class="ui-checkbox ui-checkbox-info">
				<label>
					<input type="checkbox" ng-model="checkboxModel.Francais" >
					<span><img ng-src="..\img\flags\fr111.jpg" width="20px" tooltip="Francais" tooltip-placement="bottom" >&nbsp; <span translate="header.navbar.French">French</span></span> 
				</label>
			</div>
		</div>
	</div>
	

	<div class="form-group mb0">
		<label class="col-lg-2 control-label">On request</label>
		<div class="col-lg-1" style="width:110px">
			<select id="requestState" name="request" class="form-control"
			    ng-options="option.name for option in requestData.availableOptions track by option.id"
			    ng-model="requestData.selectedOption">
			</select>
		</div>	
	</div><br><br><br>	
</form>