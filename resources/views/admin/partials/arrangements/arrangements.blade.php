
<div class="page page-form-wizard clearfix ng-scope">

    <ol class="breadcrumb breadcrumb-small">
        <li>Your Hotel</li>
        <li class="active"><a href="#/hotels"> Add New Arrangements</a></li>
    </ol>

    <div class="page-wrap panel" ng-controller="ArrangementController">
    	<div class="panel-heading"><i>Add New Arrangements</i></div>
    	<div class="panel-body">

			<div style="width:100%">
				<div ng-show="steps[0]">
					@include( 'admin.partials.arrangements.parts.form-basic' )
				</div>
				<div ng-show="steps[1]">
					@include( 'admin.partials.arrangements.parts.form-price' )
				</div>
				<div ng-show="steps[2]">
					@include( 'admin.partials.arrangements.parts.form-rooms' )
				</div>
				<div ng-show="steps[3]">
					@include( 'admin.partials.arrangements.parts.form-validity' )
				</div>
			</div>

		</div>
		<div>
			<ul class="list-unstyled wizard-tabs mb30" style="margin-left:50%">
				<li ng-class="{active: steps[0]}"> 
					<span class="text" ng-click="stepNext(0)">Basic Info</span>
					<i class="fa fa-long-arrow-right"></i>
				</li>
				<li ng-class="{active: steps[1]}">
					<span class="text" ng-click="stepNext(1)">Price and Room Availability</span>
					<i class="fa fa-long-arrow-right"></i>
				</li>
				<li ng-class="{active: steps[2]}">
					<span class="text" ng-click="stepNext(2)">Validity</span>
					<i class="fa fa-long-arrow-right"></i>
				</li>
				<li ng-class="{active: steps[3]}">
					<span class="text" ng-click="stepNext(3)">Upload Image</span>
				</li>
			</ul>
		</div>
		
    </div>

</div>
