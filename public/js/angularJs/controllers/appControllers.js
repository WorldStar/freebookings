
/*** App Controllers
------------------------------------------------ ****/

/*** App Controller ***/
ctrls.controller("AppCtrl", ["$rootScope", "$scope","spinnerService", function ($rs, $scope,spinnerService) {

$scope.getLoaded1 =  function() {

var mm = window.matchMedia("(max-width: 767px)");
    $rs.isMobile = mm.matches ? !0 : !1, $rs.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        "$apply" == phase || "$digest" == phase ? fn && "function" == typeof fn && fn() : this.$apply(fn)
    }, mm.addListener(function (m) {
        $rs.safeApply(function () {
            $rs.isMobile = m.matches ? !0 : !1
        })
    }), $scope.themeActive = "theme-zero", $scope.onThemeChange = function (theme) {
        $scope.themeActive = theme
    }, $rs.changeLayout = function () {
        $rs.isLayoutHorizontal = !$rs.isLayoutHorizontal
    }

}
}]);

/*** Head Controller ***/
ctrls.controller("HeadCtrl", ["$scope", "Fullscreen", "$rootScope", function ($scope, Fullscreen) {
    $scope.toggleSidebar = function () {
        $scope.sidebarOpen = $scope.sidebarOpen ? !1 : !0
    }, $scope.fullScreen = function () {
        Fullscreen.isEnabled() ? Fullscreen.cancel() : Fullscreen.all()
    }, $scope.toggleHorizontalNav = function () {
        $scope.isHorizontalCollapsed = !$scope.isHorizontalCollapsed
    }
}]);

/*** nav Controller ***/
ctrls.controller("NavCtrl", ["$scope", "$rootScope", function ($scope) {
    $scope.isCollapsed = !1
}]);

/*** Dashboard Conroller ***/
ctrls.controller("DashboardCtrl", ["$scope", "$rootScope", function ($scope) {
    $scope.linedata = {
        labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        series: [{name: "Earnings", data: [100, 150, 90, 40, 30, 50, 40]}, {
            name: "Downloads",
            data: [50, 100, 40, 25, 20, 35, 30]
        }]
    }, $scope.lineopts = {
        axisY: {offset: 25, labelOffset: {y: 5}},
        axisX: {showGrid: !1},
        showArea: !0,
        showLine: !1,
        showPoint: !0,
        fullWidth: !0
    }, $scope.serverpieoptions = {barColor: "#5974d9"}, $scope.serverpiepercent = 80, $scope.bouncepiepercent = 40, $scope.weathertoday = {
        icon: Skycons.PARTLY_CLOUDY_DAY,
        size: 48,
        color: "#38B4EE"
    }, $scope.forecastDetails = [{type: "Wind:", value: "7mph"}, {
        type: "Humidity:",
        value: "46%"
    }, {type: "Dew Pt:", value: "44"}, {type: "Visibility:", value: "1mi"}, {
        type: "Pressure:",
        value: "1015 mb"
    }, {type: "Precipitation", value: "55%"}], $scope.weatherweeks = [{
        icon: Skycons.PARTLY_CLOUDY_DAY,
        size: 32,
        color: "#fff",
        day: "Tue",
        temp: "34"
    }, {icon: Skycons.RAIN, size: 32, color: "#fff", day: "Wed", temp: "28"}, {
        icon: Skycons.SNOW,
        size: 32,
        color: "#fff",
        day: "Thu",
        temp: "4"
    }, {icon: Skycons.CLEAR_NIGHT, size: 32, color: "#fff", day: "Fri", temp: "40"}, {
        icon: Skycons.FOG,
        size: 28,
        color: "#fff",
        day: "Sat",
        temp: "-3"
    }, {
        icon: Skycons.SLEET,
        size: 28,
        color: "#fff",
        day: "Sun",
        temp: "18"
    }], $scope.donutdata = {
        series: [48, 17, 19, 16],
        labels: ["Chrome: 48%", "Firefox: 17%", "IE: 19%", "Other: 16%"]
    }, $scope.donutopts = {
        donut: !0,
        donutWidth: 40,
        startAngle: 320,
        total: 0,
        showLabel: !0,
        chartPadding: 25,
        labelOffset: 30,
        labelDirection: "explode"
    }, $scope.donutdraw = function (data) {
        var colors = ["#EC407A", "#7E57C2", "#A1887F", "#90A4AE"], elem = data.element._node;
        "label" == data.type && (elem.style.fill = colors[data.index]), "slice" == data.type && (elem.style.stroke = colors[data.index], elem.style["-webkit-transition"] = ".2s ease-in", elem.style.transition = ".2s ease-in", elem.addEventListener("mouseover", function () {
            elem.style["stroke-width"] = "48px"
        }), elem.addEventListener("mouseout", function () {
            elem.style["stroke-width"] = "40px"
        }))
    }, $scope.storedEmails = ["Jkae@gmail.com", "Rks@gmail.com", "dino13@yahoo.com", "streeks_sam@outlook.com", "Kangle@msn.com", "RoniScrew@gmail.com"], $scope.storedEmailsDemo = ["Rks@gmail.com", "dino13@yahoo.com"]
}]);

/*** Page Profile Controller ***/
ctrls.controller("PageProfileCtrl", ["$scope", function ($scope) {
    $scope.linedata = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"],
        series: [{data: [50, 80, 100, 90, 120, 50, 80, 56, 135, 75, 148]}]
    }, $scope.lineopts = {axisY: {offset: 25, labelOffset: {y: 5}}, axisX: {showGrid: !1, labelOffset: {x: 10}}}
}]);


ctrls.controller('OptionalEventEndDatesCtrl', function(moment) {

var vm = this;

vm.events = [{
    title: 'No event end date',
    startsAt: moment().hours(3).minutes(0).toDate(),
    type: 'info',
    draggable: true
    },
    {
    title: 'No event end date',
    startsAt: moment().hours(5).minutes(0).toDate(),
    type: 'warning',
    draggable: true
}];

vm.calendarView = 'month';
vm.viewDate = new Date();

});

ctrls.factory('DateService', function ($rootScope, $http) {
    Date.prototype.format = function(f) {
        if (!this.valueOf()) return " ";

        var weekName = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Satureday"];
        var d = this;

        return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
            switch ($1) {
                case "yyyy": return d.getFullYear();
                case "yy": return (d.getFullYear() % 1000).zf(2);
                case "MM": return (d.getMonth() + 1).zf(2);
                case "dd": return d.getDate().zf(2);
                case "E": return weekName[d.getDay()];
                case "HH": return d.getHours().zf(2);
                case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
                case "mm": return d.getMinutes().zf(2);
                case "ss": return d.getSeconds().zf(2);
                case "a/p": return d.getHours() < 12 ? "AM" : "PM";
                default: return $1;
            }
        });
    };

    String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
    String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
    Number.prototype.zf = function(len){return this.toString().zf(len);};

    var DateService = {};

    return DateService;
});


// CustomTemplatesCtrl for 1th tap
ctrls.controller('CustomTemplatesCtrl', function($scope, $rootScope, moment, calendarConfig, $http, DateService) {
	
    var vm = this;

	// weeks number in the month
	vm.getWeeksInMonth = function(month, year){
	   var weeks=[],
		   firstDate=new Date(year, month, 1),
		   lastDate=new Date(year, month+1, 0), 
		   numDays= lastDate.getDate();
	   
	   var start=1;
	   var end=7-firstDate.getDay();
	   while(start<=numDays){
		   weeks.push({start:start,end:end});
		   start = end + 1;
		   end = end + 7;
		   if(end>numDays)
			   end=numDays;    
	   }        
		return weeks;
	}

	// init dropdown control
	vm.adjust_numbers = [];
	
	// init calender
	for(var i = 0; i < 50; i++ )
	{
		vm.adjust_numbers[i] = i + 1;
	}
	vm.adjust_value = 1;
	
	vm.adjust_value_array = [];
	
	// 
    vm.events = [{
      title: 'No event end date',
      startsAt: moment().hours(3).minutes(0).toDate(),
      type: 'info'
    }, {
      title: 'No event end date',
      startsAt: moment().hours(5).minutes(0).toDate(),
      type: 'warning'
    }];

	
    vm.calendarView = 'month';
    vm.viewDate = moment().startOf('month').toDate();
	vm.caledata = {};
	
	Date.prototype.getWeekNumber = function(){
		var d = new Date(+this);
		d.setHours(0,0,0);
		d.setDate(d.getDate()+4-(d.getDay()||7));
		return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
	};

	vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
	vm.weeknumber = vm.viewDate.getWeekNumber();
	
	// rendering
	var selectInfo = document.cookie.split(';');
	var selectval = selectInfo[0].split(',');
	var fromdate = selectval[1];
	var untildate = selectval[2];
	var maxdate = selectval[3];
	var price = selectval[4];
	
	document.cookie = "selectval=0," + fromdate + "," 
									+ untildate + ","
									+ maxdate + ","
									+ price;

	
	var startDate = new Date(fromdate);
	startDate = startDate.setDate(startDate.getDate() - 1);
	var endDate = new Date(untildate);

	vm.cellModifier = function(cell) {
       //console.log(cell);
	   
		vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
		vm.weeknumber = vm.viewDate.getWeekNumber();
		for(var i =0; i < vm.weeks.length;i++)
		{
			vm.adjust_value_array[i] = 1;
		}
		
		var key = cell.date._d.toString();
		
		var caledata = vm.caledata[key];
		var state = 0;
		if( caledata == undefined )
		{
			vm.caledata[key] = {};			
			vm.caledata[key].editcount = 0;
			vm.caledata[key].count = 0;
			vm.caledata[key].adjust = false;
			vm.caledata[key].sync = true;
		}
		if(cell.isPast == false){
			
			if(cell.date > startDate & cell.date < endDate){
				vm.caledata[key].sync = true; 
				vm.caledata[key].editcount = maxdate;	
			}
			
			cell.adjust = false;
			cell.checked = false;	
			if( vm.caledata[key].adjust == false )
			{				
				if( vm.caledata[key].editcount > 0 )
				{
					if( vm.caledata[key].sync == true )
						cell.style = 'background-color: rgb(0,204,0);';// green	
					else
						cell.style = 'background-color: rgb(204,204,204);'; // grey	
				}
				else
					cell.style = 'background-color: rgb(204,204,204);';// grey	
			}
			else
			{
				cell.style = 'background-color: rgb(241, 93, 0);';// orrange	
			}
			
		}else{
			cell.style = 'background-color: rgba(230,230,230,1);';// grey	
			cell.checked = true;
		}
		// if(cell.isToday == true){
			// cell.style = "border: 1px solid rgb(241, 93, 0); background-color: rgb(204,204,204)";
		// }

		state = vm.caledata[key].state;		
		cell.editcount = vm.caledata[key].editcount;
	    cell.count = vm.caledata[key].count;
		// today
		if(cell.isToday == true){
			cell.style = "border: 1px solid rgb(241, 93, 0);" + cell.style;
		}
		
    };
	// change adjust state while select a cell
	vm.timespanClicked = function(date) {
        console.log(date);
		
		var day = date.getDate();
	
		var key = date.toString();
		var caledata = vm.caledata[key];
		
		var adjust = vm.caledata[key].adjust;
		adjust = !adjust;
		
		vm.caledata[key].adjust = adjust;

		// vm.viewDate = moment().startOf('month').toDate();
		// refresh		
		vm.events = [{
		    title: 'No event end date',
		    startsAt: moment().hours(3).minutes(0).toDate(),
		    type: 'info'
		}];	
    };
	
	vm.viewChangeClicked = function(nextView) {
        return false;  
    };
	
	vm.adjust = function()
	{
		var adjust_value = vm.adjust_value;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {
			   if( vm.caledata[k].adjust == true )
			   {
				   vm.caledata[k].editcount = adjust_value;
				   vm.caledata[k].adjust = false;
				   vm.caledata[k].sync = false;
			   }
			}
		}
	
		
		vm.viewDate = moment().startOf('month').toDate();		
	}
	
	vm.change = function()
	{
		var request = [];
		
		var adjust_value = vm.adjust_value;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {				
				vm.caledata[k].adjust = false;			
				vm.caledata[k].sync = true;		

				var date = new Date(k);			
				var data = {};
				data.date = date.format('yyyy-MM-dd');
				data.count = vm.caledata[k].editcount;	
				
				request.push(data);				
			}
		}
		
		vm.events = [{
		  title: 'No event end date',
		  startsAt: moment().hours(3).minutes(0).toDate(),
		  type: 'info'
		}];	
		
		// broadcast
		$rootScope.$broadcast('onChangeEditCount', vm.caledata);
		
//		$http({
//                method: 'POST',
//                url: '/changeaccount',
//                data: request,
//                headers: {'Content-Type': 'application/json; charset=utf-8'}
//            }).then(function(response) {
//				showTicketFilter(response);
//			}).catch(function(response) {
////				console.error('Gists error', response.status, response.data);
//			})
//			.finally(function() {
//
//			});
	}
	
	
	vm.cancel = function()
	{
		var adjust_value = vm.adjust_value;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {			    
				vm.caledata[k].adjust = false;			   
			}
		}
		
		vm.viewDate = moment().startOf('month').toDate();		
	}

});



// CustomTemplatesCtrl2 for 2th tap
ctrls.controller('CustomTemplatesCtrl2', function($scope, $rootScope, moment, calendarConfig) {
	
    var vm = this;
	// weeks number in the month
	vm.getWeeksInMonth = function(month, year){
	   var weeks=[],
		   firstDate=new Date(year, month, 1),
		   lastDate=new Date(year, month+1, 0), 
		   numDays= lastDate.getDate();
	   
	   var start=1;
	   var end=7-firstDate.getDay();
	   while(start<=numDays){
		   weeks.push({start:start,end:end});
		   start = end + 1;
		   end = end + 7;
		   if(end>numDays)
			   end=numDays;    
	   }        
		return weeks;
	}   
//	vm.adjust_price = 300;
	// refresh
    vm.events = [{
      title: 'No event end date',
      startsAt: moment().hours(3).minutes(0).toDate(),
      type: 'info'
    }, {
      title: 'No event end date',
      startsAt: moment().hours(5).minutes(0).toDate(),
      type: 'warning'
    }];

	
    vm.calendarView = 'month';
    vm.viewDate = moment().startOf('month').toDate();
	vm.caledata = {};
	
	Date.prototype.getWeekNumber = function(){
		var d = new Date(+this);
		d.setHours(0,0,0);
		d.setDate(d.getDate()+4-(d.getDay()||7));
		return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
	};

	vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
	vm.weeknumber = vm.viewDate.getWeekNumber();

	// rendering
	var selectInfo = document.cookie.split(';');
	var selectval = selectInfo[0].split(',');
	var fromdate = selectval[1];
	var untildate = selectval[2];
	var maxdate = selectval[3];
	var price = selectval[4];
	
	document.cookie = "selectval=0," + fromdate + "," 
									+ untildate + ","
									+ maxdate + ","
									+ price;
	var startDate = new Date(fromdate);
	startDate = startDate.setDate(startDate.getDate() - 1);
	var endDate = new Date(untildate);

	vm.cellModifier = function(cell) {
       //console.log(cell);
	  
		vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
		vm.weeknumber = vm.viewDate.getWeekNumber();
		
		var key = cell.date._d.toString();
			
		var caledata = vm.caledata[key];
		var state = 0;
		if( caledata == undefined )
		{
			vm.caledata[key] = {};			
//			vm.caledata[key].editcount = 300;
			vm.caledata[key].adjust = false;
			vm.caledata[key].sync = true;
		}
		if(cell.isPast == false){
			cell.editcount = vm.caledata[key].editcount;
			if(price != undefined) {
				if (cell.date > startDate & cell.date < endDate) {
					vm.caledata[key].editcount = price;
				}
				//else {
				//	vm.caledata[key].editcount = 0;
				//}
			}
			cell.show = false;
			cell.adjust = false;
//			cell.show = false;
			if( vm.caledata[key].adjust == false )
			{				
				cell.style = 'background-color: rgb(204,204,204);';// grey	
			}
			else
			{
				cell.style = 'background-color: rgb(241, 93, 0);';// orrange	
			}
			
		}else{
			cell.style = 'background-color: rgba(204,204,204,1);';// grey	
			cell.show = true;
		}

		state = vm.caledata[key].state;		
		cell.editcount = vm.caledata[key].editcount;
		// today
		if(cell.isToday == true){
			cell.style = "border: 1px solid rgb(241, 93, 0);" + cell.style;
		}
		
    };
	// change adjust state while select a cell
	vm.timespanClicked = function(date) {
        console.log(date);
		
		var day = date.getDate();
	
		var key = date.toString();
		var caledata = vm.caledata[key];
		
		var adjust = vm.caledata[key].adjust;
		adjust = !adjust;
		
		vm.caledata[key].adjust = adjust;

		// vm.viewDate = moment().startOf('month').toDate();	
		vm.events = [{
		  title: 'No event end date',
		  startsAt: moment().hours(3).minutes(0).toDate(),
		  type: 'info'
		}];	
    };
	
	vm.viewChangeClicked = function(nextView) {
        return false;  
    };
	
	$scope.changeprice = function()
	{
		var adjust_price = vm.adjust_price;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {
			   if( vm.caledata[k].adjust == true )
			   {
				   vm.caledata[k].editcount = adjust_price;
				   vm.caledata[k].adjust = false;
				   vm.caledata[k].sync = false;
			   }
			}
		}
		
		vm.viewDate = moment().startOf('month').toDate();		
	}
	
	vm.change = function()
	{
		var request = {};
		
		var adjust_price = vm.adjust_price;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {				
				vm.caledata[k].adjust = false;			
				request[k] = vm.caledata[k].editcount;		
				vm.caledata[k].sync = true;				
			}
		}
		
		vm.events = [{
		  title: 'No event end date',
		  startsAt: moment().hours(3).minutes(0).toDate(),
		  type: 'info'
		}];	
		
		// broadcast
		$rootScope.$broadcast('onChangePrice', vm.caledata);

		
		// $http({
                // method: 'POST',
                // url: '/changeaccount',
                // data: request,
                // headers: {'Content-Type': 'application/json; charset=utf-8'}
            // }).then(function(response) {
				// showTicketFilter(response);
			// }).catch(function(response) {
				// console.error('Gists error', response.status, response.data);
			// })
			// .finally(function() {

			// });
	}
	
	vm.cancel = function()
	{
		var adjust_price = vm.adjust_price;
		
		for (var k in vm.caledata) {
			if (vm.caledata.hasOwnProperty(k)) {			    
				vm.caledata[k].adjust = false;			   
			}
		}
		
		vm.viewDate = moment().startOf('month').toDate();		
	}
	

});

// CustomTemplatesCtrl3 for 3th tap
ctrls.controller('CustomTemplatesCtrl3', function($scope, moment, calendarConfig) {
	var vm = this;
	// weeks number in the month
	vm.getWeeksInMonth = function(month, year){
	   var weeks=[],
		   firstDate=new Date(year, month, 1),
		   lastDate=new Date(year, month+1, 0), 
		   numDays= lastDate.getDate();
	   
	   var start=1;
	   var end=7-firstDate.getDay();
	   while(start<=numDays){
		   weeks.push({start:start,end:end});
		   start = end + 1;
		   end = end + 7;
		   if(end>numDays)
			   end=numDays;    
	   }        
		return weeks;
	}   
	// refresh
    vm.events = [{
        title: 'No event end date',
        startsAt: moment().hours(3).minutes(0).toDate(),
        type: 'info'
    }, {
        title: 'No event end date',
        startsAt: moment().hours(5).minutes(0).toDate(),
        type: 'warning'
    }];

	
    vm.calendarView = 'month';
    vm.viewDate = moment().startOf('month').toDate();
	vm.caledata = {};
	
	Date.prototype.getWeekNumber = function(){
		var d = new Date(+this);
		d.setHours(0,0,0);
		d.setDate(d.getDate()+4-(d.getDay()||7));
		return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
	};

	vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
	vm.weeknumber = vm.viewDate.getWeekNumber();
	
	// rendering
	var selectInfo = document.cookie.split(';');
	var selectval = selectInfo[0].split(',');
	var fromdate = selectval[1];
	var untildate = selectval[2];
	var maxdate = selectval[3];
	var price = selectval[4];
	
	document.cookie = "selectval=0," + fromdate + "," 
									+ untildate + ","
									+ maxdate + ","
									+ price;
	var startDate = new Date(fromdate);
	startDate = startDate.setDate(startDate.getDate() - 1);
	var endDate = new Date(untildate);
	
	vm.cellModifier = function(cell) {
       //console.log(cell);
	  
		vm.weeks = vm.getWeeksInMonth(vm.viewDate.getMonth(), vm.viewDate.getYear() + 1900);
		vm.weeknumber = vm.viewDate.getWeekNumber();
		
		var key = cell.date._d.toString();
			
		var caledata = vm.caledata[key];
		var state = 0;
		if( caledata == undefined )
		{
			vm.caledata[key] = {};			
			vm.caledata[key].count = 0;
			vm.caledata[key].money = 300;
			vm.caledata[key].adjust = false;
			vm.caledata[key].sync = true;
		}

		cell.style = 'background-color: rgb(204,204,204);';// grey	

		state = vm.caledata[key].state;		
	    cell.count = vm.caledata[key].count;
		cell.money = vm.caledata[key].money;
		cell.show = true;

		if(cell.isPast == false){
			
			if(cell.date > startDate & cell.date < endDate){
				cell.count = maxdate;
				cell.money = price;
//				cell.show = false;
			}else{
				vm.caledata[key].editcount = 0;
//				cell.show = true;
			}
			cell.show = false;
		}

		// today
		if(cell.isToday == true){
			cell.style = "border: 1px solid rgb(241, 93, 0);" + cell.style;
		}
		
    };
	
	$scope.$on('onChangeEditCount', function(event, args) {

		var caledata = args;
		
		for (var k in caledata) {
			if (caledata.hasOwnProperty(k)) {	
				if( vm.caledata[k] == undefined )
				{
					vm.caledata[k] = {};			
					vm.caledata[k].count = 2;
					vm.caledata[k].money = 300;
					vm.caledata[k].adjust = false;
					vm.caledata[k].sync = true;
				}
					
				vm.caledata[k].count = caledata[k].editcount;				
			}
		}
		
		vm.events = [{
		  title: 'No event end date',
		  startsAt: moment().hours(3).minutes(0).toDate(),
		  type: 'info'
		}];	
		
	});
	
	$scope.$on('onChangePrice', function(event, args) {

		var caledata = args;
		
		for (var k in caledata) {
			if (caledata.hasOwnProperty(k)) {	
				if( vm.caledata[k] == undefined )
				{
					vm.caledata[k] = {};			
					vm.caledata[k].count = 2;
					vm.caledata[k].money = 300;
					vm.caledata[k].adjust = false;
					vm.caledata[k].sync = true;
				}
					
				vm.caledata[k].money = caledata[k].editcount;				
			}
		}
		
		vm.events = [{
		  title: 'No event end date',
		  startsAt: moment().hours(3).minutes(0).toDate(),
		  type: 'info'
		}];	
		
	});

});

// angular
  // .module('mwl.calendar.docs') //you will need to declare your module with the dependencies ['mwl.calendar', 'ui.bootstrap', 'ngAnimate']
ctrls.controller('KitchenSinkCtrl', function(moment, alert) {

    var vm = this;

    //These variables MUST be set as a minimum for the calendar to work
    vm.calendarView = 'month';
    vm.viewDate = new Date();
    vm.events = [
      {
        title: 'An event',
        type: 'warning',
        startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
        endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate(),
        draggable: true,
        resizable: true
      }, {
        title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
        type: 'info',
        startsAt: moment().subtract(1, 'day').toDate(),
        endsAt: moment().add(5, 'days').toDate(),
        draggable: true,
        resizable: true
      }, {
        title: 'This is a really long event title that occurs on every year',
        type: 'important',
        startsAt: moment().startOf('day').add(7, 'hours').toDate(),
        endsAt: moment().startOf('day').add(19, 'hours').toDate(),
        recursOn: 'year',
        draggable: true,
        resizable: true
      }
    ];

    vm.toggle = function($event, field, event) {
      $event.preventDefault();
      $event.stopPropagation();
      event[field] = !event[field];
    };
	
});


ctrls.controller('PriceTabCtrl', function($scope, moment, calendarConfig) {
	
    var vm = this;

	$scope.tabSelected1 = function() {
		calendarConfig.templates.calendarMonthCell = 'customMonthCell.html';
	}
	
	$scope.tabSelected2 = function() {
		calendarConfig.templates.calendarMonthCell = 'customMonthCell2.html';
	}

	$scope.tabSelected3 = function() {
		calendarConfig.templates.calendarMonthCell = 'customMonthCell3.html';
	}
	
	$scope.$on('$destroy', function() {
      calendarConfig.templates.calendarMonthCell = 'mwl/calendarMonthCell.html';
    });
});



