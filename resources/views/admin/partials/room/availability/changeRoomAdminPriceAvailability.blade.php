	<script id="customMonthCell3.html" type="text/ng-template">									
		<div class="cal-month-day"  style="@{{day.style}}">
			<span
				class="pull-right"
				data-cal-date
				ng-bind="day.label">
			</span>

			<small ng-hide="day.show">
			
				<p style="position: absolute; bottom: 30px; left: 10px">
					<span ng-bind="day.count"></span> 
				</p>
				
				<p style="position: absolute; bottom: 5px; left: 10px">
					<span style="float:left;font-size:13px;">€</span>
					<span ng-bind="day.money"></span> 
				</p>
			
			</small>	
		</div>	
	</script>
	
	<b>
		Administration Prices and availability from de kamer.</i>
		<br> <br>
	</b>
	<h2 class="text-center"><span ng-bind="vm.calendarTitle"></span></h2> 										
	
	@include('admin.pages.calendarControls')
	<div class="div_changeprice">
	
		<div style="display: -webkit-flex;display: flex;">
			<div style="-webkit-flex: 1;flex: 1; -webkit-flex-direction: column; flex-direction: column;">
				<section style="height:29px;background-color:#fafafa">
					<p style="margin-left:10px; padding-top:5px"><b>Week</b></p>
				</section>
				<section style="height:101px; background-color:#fafafa;border-color:#ffffff" ng-repeat="week in vm.weeks track by $index">
					<p style="margin-left:10px; padding-top:10px"><b>@{{vm.weeknumber + $index}}</b></p>
					<p style="margin-left:10px;margin-top:-5px"><b>Beschikbaar</b></p>
				</section>																								
				
			</div>
			<div style="-webkit-flex: 7;flex: 7;">
				
				<mwl-calendar
					events="vm.events"
					view="vm.calendarView"
					view-date="vm.viewDate"
					cell-is-open="vm.isCellOpen"
					view-title="vm.calendarTitle"
					on-timespan-click="vm.timespanClicked(calendarDate)"
					cell-modifier="vm.cellModifier(calendarCell)"
					on-view-change-click="vm.viewChangeClicked(calendarCell, calendarNextView)"
					slide-box-disabled="true"
					on-event-click="vm.eventClicked(calendarEvent)"
					on-event-times-changed="vm.eventTimesChanged(calendarEvent); calendarEvent.startsAt = calendarNewEventStart; calendarEvent.endsAt = calendarNewEventEnd"
					edit-event-html="'<i class=\'glyphicon glyphicon-pencil\'></i>'"
					>
				</mwl-calendar>
			</div>	
		</div>
	</div><br>
	<div>
		$xx,xx=Last minute prijs &nbsp;&nbsp;&nbsp; $xx,xx=Vroegboek korting periode &nbsp;&nbsp;&nbsp; $xx,xx=Korting periode
	</div>
